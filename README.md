# Poe
Poe is a Python module to set, run and analyse fit of 3D mixture models for Galactic kinematics applied to observed transverse velocity obtained from proper motion. The fitting machinery is based on the Python module  [emcee](https://emcee.readthedocs.io/en/stable/).
Poe has been used in the paper "Chemo-kinematics of the Gaia RR Lyrae: the halo and the disc" by Iorio&Belokurov, 2020.
The function are commented but there is not a proper guide or a wiki. Below there  is a short description of the the package. An example of a fit script is inside the folder script. 
If you want to make a similar analysis to the one showed in Iorio&Belokurov, 2020 or if you want to reproduce the results of the paper, please send me an email to giuliano.iorio.astro@gmail.com, I will be happy to help and collaborate.

## Installation
Just run `python setup.py install`

### Dependencies 
Poe depends on the following packages:


- Numpy
- Scipy
- Pandas
- Matplotlib
- Astropy
- [emcee](https://emcee.readthedocs.io/en/stable/)
- [Corner](https://github.com/giulianoiorio/corner.py)
- [Psutil](https://pypi.org/project/psutil/)
- [Gastro](https://github.com/giulianoiorio/GAstro) 
    


## Usage
**NOTICE** All the velocities are assumed to be in **km/s**, all the distances and (non angular) coordinates in **kpc** and all the angular coordinates in **degrees**.

Here we report a guided simple example of a kinematic fit. A more detailed explanations of the classed and functions used can be found in the next section
1. Create an instace of the class Observed:

`    	Obs      =  Observed(l=l, b=b, theta=theta, phi=phi, Vl=Vl, Vb=Vb, Vl_err=Vl_err, Vb_err=Vb_err, C_Vl_Vb=C_Vl_Vb,)`

    l and b are the Galactic coordinates of the objects to fit (1D numpy array), theta and phi are the Galactocentric spherical angles, Vl and Vb the velocities along l and b in km/s already corrected for the sun motion, Vl_err and Vb_err their errors and C_Vl_Vb their correlation coefficients.

2. Define the Kinematic models and then mix them with FitComponents

`	
    model1=FitHalo('A', prior_range={'Sr':PositiveGaussian(100,100), 'Stheta':PositiveGaussian(100,100), 'Sphi':PositiveGaussian(100,100)})
	model2=FitDisc('BI', prior_range={'Vphi':Uniform(10,300), 'SR':PositiveGaussian(30,30)})
	model=FitComponents(model1,model2)
`

    The final Model will be the mixture of two kinematic components: an isotropic model in spherical coordinates and zero stream velocities (model1),  and an anistropic model in cylindrcal coordinates with non zero azimuthal velocity. See next section for details about the kinematic models.

3.  The fit! 

    `output   =  fitsample(Obs, fitmodel, nwalkers=nwalkers, nburn=nburn, nstep=nstep, nproc=1, progress=False)`

    The fit is perfomed passing the object Obs and fitmodel to the function fitsample adding parameters for the MCMC chain. The return of the fit is an objecte storing all the results of the fit (and even more) (see next section for details).

4. Make some plot! 

```
MCMCplot =  MCMC_Plot(output)
MCMCplot.plot_all("fit_results",Obs=Obs)
```

Here we create the object MCMCplot from the class MCMC_Plot giving direcly the object output. Then we use a method of the class to save a number of default plots in the directory fit_results.

5. Print some infos 

```
output.save("fit_results")
output.print_log(file="fit_results"+'/info.log')
```

Here we use the object output to save  infos and logs in the folder fit_results 


A complete example of a script for fitting binned data in parallel can be found in the script folder.





## Package Overview

### Classes and Functions
The most important (for the user) classes and functions are:

- **Observed** from poe.matrix: this class handle the observed variabled needed to performe the kinematic fit. The 
paremter are:

    - param(Mandatory) **l**: Galactic sky longitude [deg]
    - param(Mandatory) **b**: Galactic sky latitude [deg]
    - param(Mandatory) **theta**:  Galactic latitude [deg]
    - param(Mandatory) **phi**:  Galactic longitude [deg]
    - param(Mandatory) **Vl**:  Velocity along l (already corrected for the Sun Motion) [kms/s]
    - param(Mandatory) **Vb**:  Velocity along b (already corrected for the Sun Motion) [kms/s]
    - param(Mandatory) **Vl_err**:  Velocity error along l  [kms/s]
    - param(Mandatory) **Vb_err**: Velocity error along b  [kms/s]
    - param(Mandatory) **C_Vl_Vb**:  correlation coefficent Vl_Vb_error
    - param(default=None) **R**: Galactic cylindrical radius
    - param(default=None) **z**: Galactic cylindrical height
    - param(default=None) **R_err**: error on Galactic cylindrical radius
    - param(default=None) **z_err**: error on Galactic cylindrical height

- **fitsample** from poe.fitutility: this function does the actual kinematic fit. The parameters are:

    - param(Mandatory) **observed**: An instance of the class poe.matrix.Observed
    - param(Mandatory) **fitmodel**: An instance of the class  poe.fitmodel.FitComponents
    - param(default=300) **nwalkers**:  Number of walkers to use in the MCMC (see emcee documentation)
    - param(default=100) **nburn**:  Number of MCMC steps used as burning-in (they are discarded in the final sample, see emcee documentation))
    - param(default=400) **nstep**:  Number of MCMC steps (see emcee documentatio)
    - param(default=None) **vdisp_order**:  If not None, it could be the string ascending or descending. If ascending the velocity disperion (all the three components) of the fitted components are forced to be in ascending order. 
    - param(default=None) **vphi_order**:  Same as vdisp_order but for the Vphi parameter. If ascending the velocity disperion (all the three components) of the fitted components are forced to be in ascending order. 
    - param(default=1) **nproc**:  Number of processor to use in the MCMC exploration (see emcee documentation)
    - param(default=False) **progress**: It true show the progress bar during MCMC exploration (see emcee documentation)
    - return An instance of the class poe.fitutility.MCMC_output
Some preliminary tests show that for the used likelihood setting nproc>1 is slower than just using a sequential approach. n order to exploit parallelism is better to parallelise the application of fit to different bounch of data instead of parallelasing the MCMC exploration. The param vdisp_order and vphi_order have been added to help label identificability among the different kinematic component. However, I found it is better using smart priors for the kinematic parameters and leave these parameters to the default None. This function performs the fit and return in ouput an instance of the clas poe.fitutility.MCMC storing all the outputs of the fit (see below).

- **MCMC_Output** from poe.fitutility: This class handles the output of the kinematic fit. It is not intented to be called directly by the user,  instead an instance it is returned as return from the function fitsample (see above). The attributes and methods that can be accessed are:


    - **param_names**:  Name of all the fitted parameters
    - **dimension**: number of fitted parameters
    - **samples**:  flattened version of the chains sample by the MCMC
    - **logprob**: flattened version of the logprob obtained for each elemente of the MCMC chains
    - **MAP **(Maximum a Posteriori): this is a tuple, with the first number indicating the value of the logprob at MAP, and the second is an array containing the MAP parameters
    - **BIC** (Bayesian Inference Criterion): this is a tuple, with the first number indicating the value of the BIC, and the second is an array containing the MAP parameters used to estimate the BIC
    - **Qp**: this is the a-posteriori distributions of each point in the sample to belong to a given kinematic component. It is an array with dimension N,M,L where N is the number of fitted stars, M is the number of compoenets and L is the number of MCMC steps.
    - **q_particle_table**: this is a pandas dataframe containing the MAP, 50th, 16th, 84th percentile for the a-posteiori likelihood to belong to a given component for each star in the fitted sample.
    - **beta**: Array containing the distribution of the anisotropy parameter for each component.
    - **MAP_kinematic_model**: this is an instance of the class FitComponents in poe.fitmodel containing the models with the parameters giving the MAP of the fit. 
    - **median_kinematic_model** Same as MAP_kinematic_model but with all the parameters fixed to the median of their posterior distribution.
    - **percentile(q)**: Return a pandas dataframe with the percentiles given in input (could be a list) of all the fitted parameters.
    - **acceptance_fraction**: this a tuple containing the mean and the std of the acceptance_fraction of the MCMC sampling (see emcee documentation).
    - **autocorrelation**: return a list containing the integrated autocorrelation MCMC  for each fitted parameter (see emcee documentation). The number of steps has to be larger than the  integrated autocorrelation for all the parameters. Notice also that estimate of integrated autocorrelation for sample smaller than 50 times the integrated autocorrelation is likyely unreliable (see emcee documentation)

- **MCMC_Plot** from poe.fitutility: This is a class handling the plot of the MCMC kinematic fit results. It can be called simply as MCMC_Plot(output) where output is an instance  of the class MCMC_Output (see above).  The main functions are:

     - **corner_plot(save=None)**: Plot the classical corner or triangle plots of all the fitted parameters.
     - **trace_plot(save=None)**: Classical trace plot the MCMC walkers.
     - **autocorr_plot(save=None)**: Autocorrelation plots for diagnostic (see emcee documnetation for more information)
      In the plot the y-axis indicate the normalised autocorrelation, it should tends to 0 for all the walkers and parameters
      (different walker are rapresent by different colors, the black curves is obtained using all the walkers as a   
      whole. The black vertical line rapresent the integreted autocorrelation. It should be much smaller than the extensions 
      of the chains. A grey shadow area rapsent the the region for Lag from 0 to 50*autocorr, if most of the curves are inside the
      grey area, the estimate of the autocorrelation is not reliable (see emcee documentation), in this case longer chains should
       be used. 
     - **sample_summary(Obs, save=None)**: Plot a summary comparing the fitted data and the  fitted model. Obs is  the instance of 
      the class Observed (see above) used to the fit the model that generated the output (see above). The summary plot has the 
      following panels:
         - first row: distribution of stars in l,b (left), distribution of the star in Rcyl, z (right)
         - second row: istribution of the star in Vl, Vb + distribution of the MAP model (left), residual observed-MAP model in
             Vl,Vb (middle), distribution of  Vl for data (black), MAP model (blue), Median model (orange), (right).
         - third row:  distribution of the star in Vl, Vb + distribution of the Median model (left), residual observed-Median
             model in Vl,Vb (middle), distribution of  Vb for data (black), MAP model (blue), Median model (orange) (right)
In all the above functions If save is not None and it is a string the plot is saved with this save name.                      There are also other possible keyword parameters to tune the plot proprties (see the source code).
                                                                                                          
                                                                       
### Kinematic Models        
The core of the package is rapresented by  the Kinematic Models that can be fitted. 
All the kinematic models are class derived from the template class FitModel_Skeleton in poe.fitmodel. 
There are three 3D models:
- **FitHalo**, 3D model based on Spherical Galctocentric coordinates, the parameters are:  the three velocity centroids _Vr, Vtheta, Vphi_, the three velocity dispersions _Sr, Stheta, Sphi_ and the three correlation coefficients _C_r_theta, C_r_phi, C_theta_phi_;
- **FitGiano**, 3D model based on Spherical Galctocentric coordinates. This model is similar to the FitHalo except in the radial dimension, here the velocity distribution is composed by two gaussian distributions with the same velocity dispersion _Sr_ but with centroid at |_Lr_| and -|_Lr_|, respectively,  the parameters are:  the two velocity centroids _Vtheta, Vphi_, the pseudo radial centroid _Lr_,  the three velocity dispersions _Sr, Stheta, Sphi_ and the three correlation coefficients _C_r_theta, C_r_phi, C_theta_phi_;
- **FitDisc**, 3D model based on Spherical Galctocentric coordinates, the parameters are: the three velocity centroids _VR, Vz, Vphi_, the three velocity dispersions _SR, Sz, Sphi_ and the three correlation coefficients _C_R_z, C_R_phi, C_z_phi_;

There is also a on-sky 2D mode used mostly to take into account a "noise" background:
- **FitSkyTanBackground**, 2D model based on sky Galactic coordinate, the paremeters are: the two velocity centroids _Vl,Vb_, the two velocity dispersions _Sl,Sb_ and the correlation coefficient _C_Vl_Vb_.

Finally, there is an auxiliary Kinematic Models, actually used in fit called:
- **FitComponents**, it is just an auxiliary class contaning all the N kinematic models used in the fit. An istance is created simply as `fitmod=FitComponents(mod1,mod2,mod3,...)`  where modx are all instance of the Kinematic Models (FitHalo, FitGiano,FitDisc,FitSkyTanBackground). This instance is passed to the function fitsample (see above).

An Kinematic model is instanced with the following parameters:
- (Mandatory)**option**:  string defining the option for the given model. Each model define a number of free parameters and conditions on the other parameters. All the options for the 3D and 2D kinematic models are reported in the tables below.
- (Default {})**prior_range**: dictionary with keys that are the names of the free parameters and the value that are one of the density distribution defined in poe.distribution (see the source code for details).  This parameters is used to define the priors on the fitted parameters. Default priors are assided to  parameters without a defined prior: Gaussian(0,500) for the velocities [km/s], PositiveCauchy(0,500) [km/s] for the velocity dispersions and Uniform(-1,1) for the correlation parameters.
- (Default {})**fixed_param**:  dictionary with keys that are the names of the fixed parameters and value that are float number. Each non fitted parameters is fixed to the related valued in the dictionary during the fit.  Default values are assigned to the parameter without a defined value: 0 for the Velocities, 100 km/s for the Velocity dispersions  and 0 for the correlation coefficients. 
E.g.: 
```
    model_1 = FitHalo("B", prior_range={"Vphi": Gaussian(0,100), "Sr": PositiveCauchy(100,50), "Stheta":  PositiveCauchy(30,50)},
    fixed_param={"Vr":100})
```
defines a FitHalo model (3D model in spherical coordinates) with option B (_Vphi_ and  three velocity dispersions as free parameters, see below), the prior of the fitteged parameters are Gaussian with centroid 0 and dispersion 100 km/s for _Vphi _, 
a PositiveCauchy (Cauchy distribution with boundary at 0)  with  parameter 100 km/s and 50 km/s  for _Sr_, PositiveCauchy with  parameter 30 km/s and 50 km/s  for _Stheta_, the other free parametr (_Sphi_) has default prior (PositiveCauchy with parameter 0 and 500 km/s). The non fitted parameter _Vr_ is set to  100 km/s, while the other are fixed to the default values (_Vtheta_=0 and correlation parameters all equal to 0).

Option summary for 3D Kinematic models 
| Model Option | Free parameters | Other condisions |
| ------ | ------ | ------ |
| 0 | No free parameters | | 
| A | the three velocity dispersions| | 
| AI | isotropic case, _Sr_ (_SR_)| _Sphi_=_Stheta_=_Sr_ (_Sphi_=_Sz_=_SR_) |
| AT | _Sr_ (_SR_) and _Stheta_ (_Sz_) | _Sphi_=_Stheta_ (_Sphi_=_Sz_)  |
| B |  _Vphi_,  three velocity dispersions | | 
| BI | _Vphi_, _Sr_ (_SR_) | _Sphi_=_Stheta_=_Sr_ (_Sphi_=_Sz_=_SR_) | 
| BT | _Vphi_, _Sr_ (_SR_) and _Stheta_ (_Sz_) | _Sphi_=_Stheta_ (_Sphi_=_Sz_) | 
| C | the three velocity dispersions and the three correlation coefficients|  | 
| D | _Vr_ (_Lr_ or _VR_),  three velocity dispersions | | 
| DT | _Vr_ (_Lr_ or _VR_), _Sr_ (_SR_) and _Stheta_ (_Sz_)  | _Sphi_=_Stheta_ (_Sphi_=_Sz_) | 
| DR | _Vr_ (_Lr_ or _VR_), _Vphi_, three velocity dispersions  |  | 
| DTR | _Vr_ (_Lr_ or _VR_), _Vphi_, _Sr_ (_SR_) and _Stheta_ (_Sz_)  | _Sphi_=_Stheta_ (_Sphi_=_Sz_) | 
| E | _Vphi_, three velocity dispersions, three correlation coefficients  | | 
| F | _Vr_ (_Lr_ or _VR_),_Vphi_, three velocity dispersions, three correlation coefficients| | 
| G | All the parameters | | 

Option summary for 2D Kinematic models 
| Model Option | Free parameters | Other condisions |
| ------ | ------ | ------ |
| 0 | No free parameters | | 
| A | the two velocity dispersions| | 
| AI | isotropic case, _Sl_ | _Sb_=_Sl_ |
| AM | the two velocity dispersions | _Vl_ and  _Vb_ fixed to the mean value of the data |
| AIM |  _Sl_ | _Sb_=_Sl_, _Vl_ and  _Vb_ fixed to the mean value of the data | 
| B | _Vl_, _Vb_ |  | 
| C | _Sl_,_Sb_,_C_Vl_Vb_  | | 
| CM | _Sl_,_Sb_,_C_Vl_Vb_ | _Vl_ and  _Vb_ fixed to the mean value of the data | 
| D | All the parameters  | | 
| DI | _Vl_, _Vb_, _Sl_ ,_C_Vl_Vb_  | _Sb_=_Sl_  | 


