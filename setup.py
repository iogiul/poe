from distutils.core import setup
import os
import sysconfig
import sys

#NOTE, emcee should be in version 3.0rc2
setup(
		name='Poe',
		version='0.8.dev0',
		author='Giuliano Iorio',
		author_email='',
		url='',
		packages=['Poe','Poe/src'],
        install_requires=['numpy',],
		zip_safe=False
)
