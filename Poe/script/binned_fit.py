import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
from GAstro.rand import  consistent_skypoint_cartesian
from Poe.kinematic import Halo, Disc, SkyTanBackground
from Poe.matrix import Observed
from Poe.fitmodel import FitHalo, FitDisc, FitGiano, FitSkyTanBackground, FitComponents
from Poe.distribution import PositiveGaussian, Gaussian, Uniform, TruncatedGaussian
from Poe.fitutility  import fitsample, MCMC_Plot
import astropy.io.fits as ft
import os
import time
from multiprocessing import Pool
from functools import partial
import sys
from joblib import Parallel, delayed
import argparse

def make_file_dat(idx, Obs, output, file=None):

	#create return
	percent    =    output.percentile(q=(0.5,0.16,0.84))
	MAP        =    output.MAP
	pnames     =    output.param_names
	Nparam     =    output.dimension
	Ncomponents =  output.fitmodel.Ncomponents
	beta       =    output.beta

	ret = np.zeros((Nparam+Ncomponents)*4+7)

	ret[0] = np.mean(Obs.R)
	ret[1] = np.std(Obs.R)
	ret[2] = np.mean(np.abs(Obs.z))
	ret[3] = np.std(np.abs(Obs.z))
	ret[4] = output.Nsample

	i=5
	for j,name in enumerate(pnames):
		ret[i]=MAP[1][j]
		ret[i+1:i+4]=percent[name].values
		i=i+4


	idxmax = np.argmax(output.logprob)

	for j in range(Ncomponents):

		ret[i]=beta[idxmax][j]
		ret[i+1:i+4]=np.percentile(beta[:,j],q=(50,16,84))
		i = i + 4

	ret[-2]=MAP[0]
	ret[-1]=output.BIC[0]

	if file is not None:
			ret_tmp=np.r_[ idx, ret ]
			header   = '0-idxbin 1-R 2-R_err 3-z 4-z_err 5-Nsample '
			fmt_h = '%i %.3e %.3e %.3e %.3e %i '
			sub_head = ['_map','_50','_16', '_84']

			if model.Ncomponents==1:  parnames = list(model.param_names[1:])+['beta_%i'%(i+1) for i in range(model.Ncomponents)]
			else: parnames = list(model.param_names)+['beta_%i'%(i+1) for i in range(model.Ncomponents)]

			i=6
			for name in parnames:
				for subname in sub_head:
					header+='%i-%s%s '%(i,name,subname)
					fmt_h+='%.3e '
					i+=1



			header+='%i-Log %i-BIC'%(i+1,i+2)
			fmt_h+='%.3e %.3e'


			np.savetxt(file, np.array([ret_tmp,]), header=header, fmt=fmt_h)


	return ret



def _fit_bin(bin_idx, filename, fitmodel, outdir='fit', colname='idxbin_v_10', nwalkers=50, nstep=6000, nburn=300, vdisp_order=False):

	t = ft.open(filename)
	data = t[1].data
	idx = data[colname]==bin_idx
	data = data[idx]

	l = data['l']
	b= data['b']
	theta = data['theta']
	phi = data['Phi']
	Vl = data['Vl_c']
	Vb = data['Vb_c']
	Vl_err = data['Vl_c_err']
	Vb_err = data['Vb_c_err']
	C_Vl_Vb = data['p_Vl_c_Vb_c']
	z = data['z']
	z_err = data['z_err']
	R = data['Rcyl']
	R_err = data['Rcyl_err']

	t.close()
	del t

	#FIT
	Obs      =  Observed(l=l, b=b, theta=theta, phi=phi, Vl=Vl, Vb=Vb, Vl_err=Vl_err, Vb_err=Vb_err, C_Vl_Vb=C_Vl_Vb, R=R, z=z, R_err=R_err, z_err=z_err)
	output   =  fitsample(Obs, fitmodel, nwalkers=nwalkers, nburn=nburn, nstep=nstep, vdisp_order=vdisp_order, nproc=1, progress=False)
	MCMCplot =  MCMC_Plot(output)

	#save plot
	outdir=outdir+'/bin_%i'%bin_idx
	if not os.path.exists(outdir): os.makedirs(outdir)
	MCMCplot.plot_all(outdir,Obs=Obs)
	output.save(outdir)
	output.print_log(file=outdir+'/info.log')

	ret=make_file_dat(bin_idx, Obs, output, file=outdir+'/data_tab.txt')

	return ret



if __name__=='__main__':

	#args = parser.parse_args()






	fname   = 'RRabc_r40_halo_qmap09_q1605_radial.fits'
	t = ft.open(fname)
	data = t[1].data
	outdir = 'fit_1comp_halo_radial_idx35'
	vdisp_order=None

	#nwalkers = 50
	#nburn = 5000
	#nstep = 30000
	#nproc = 26

	nwalkers = 50
	nburn = 5000
	nstep = 50000


	nproc = 5
	idx_v = 35

	idx_list = np.unique(data['idxbin_v_%i'%idx_v])
	idx_list=idx_list[:]


	model1=FitHalo('B', prior_range={'Vphi':Gaussian(0,100), 'Sr':PositiveGaussian(0,200), 'Stheta':PositiveGaussian(0,200), 'Sphi':PositiveGaussian(0,200)})
	model=FitComponents(model1,)


	_partial_fit = partial(_fit_bin, filename=fname, fitmodel=model, outdir=outdir, colname='idxbin_v_%i'%idx_v, nwalkers=nwalkers, nstep=nstep, nburn=nburn, vdisp_order=vdisp_order)


	if nproc==1:
		ret=[]
		for idx in idx_list:
			ret.append(_partial_fit(idx))
	else:
		if sys.version_info>=(3,7):
			t1 = time.time()
			with Pool(nproc) as pool:
				ret=pool.map(_partial_fit,idx_list)
			t2 = time.time()
		else:
			t1 = time.time()
			ret = Parallel(n_jobs=nproc)(delayed(_partial_fit)(i) for i in idx_list)
			t2 = time.time()


	print('Parallel Multiproc multi')
	print('Time',t2-t1)


	ret=np.c_[ idx_list, ret ]
	header   = '0-idxbin 1-R 2-R_err 3-z 4-z_err 5-Nsample '
	fmt_h = '%i %.3e %.3e %.3e %.3e %i '
	sub_head = ['_map','_50','_16', '_84']

	if model.Ncomponents==1:  parnames = list(model.param_names[1:])+['beta_%i'%(i+1) for i in range(model.Ncomponents)]
	else: parnames = list(model.param_names)+['beta_%i'%(i+1) for i in range(model.Ncomponents)]

	i=6
	for name in parnames:
		for subname in sub_head:
			header+='%i-%s%s '%(i,name,subname)
			fmt_h+='%.3e '
			i+=1

	header+='%i-Log %i-BIC'%(i+1,i+2)
	fmt_h+='%.3e %.3e'


	np.savetxt(outdir+'/final_tab.txt',ret, header=header, fmt=fmt_h)
