import numpy as np
import logging

def logger(className):
	return logging.getLogger(__name__+':'+className)

class RMatrixCore:

	def __init__(self, l, b, theta, phi, degree=True, frame_orientation='left'):
		"""
		General class to define the basis for a Rotation Matrix to transform a given coordinate system to a sky spherical one:
		(Vlos, Vl, Vb).T = R dot (V_i, V_j, V_k)
		:param l: Sky l coords
		:param b:  Sky b coords
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param degree: If True angles are in degrees
		:param frame_orientation: Orientation of the Galactic Cartesian system (left or right)
		"""
		self.clog = logger(self.__class__.__name__)

		if degree:
			self.l, self.b, self.phi, self.theta = np.atleast_1d(np.radians(l)), np.atleast_1d(np.radians(b)), np.atleast_1d(np.radians(phi)), np.atleast_1d(np.radians(theta))
		else:
			self.l, self.b, self.phi, self.theta = np.atleast_1d(l), np.atleast_1d(phi), np.atleast_1d(phi), np.atleast_1d(theta)

		self.frame_orientation = frame_orientation
		if self.frame_orientation=='left':  self.x_axis_sign=-1
		elif self.frame_orientation=='right': self.x_axis_sign=1
		else: raise ValueError('Frame orientation %s not valid. Valid options are left or right'%self.frame_orientation)

		self.N=len(self.theta)
		self._R=None
		self._A=None
		self._Ainv=None

	def _make_R(self):
		"""
		Function that create the Rotation matrix using the l,b, theta, phi coordinates. It has to be defined
		in the derived specialised classes.
		:return:
		"""
		raise NotImplementedError('Rule to make the Rotation Matrix are not implemented for this class')

	def _make_A(self):
		"""
		Function that create the matrix A. It has to be defined
		in the derived specialised classes.
		:return:
		"""
		self.clog.debug('Setting A Matrix')
		self._A = np.matmul(self.RTanT, self.RTan)

		return 0

	def _make_Ainv(self):
		"""
		Function that create the inverse matrix A. It has to be defined
		in the derived specialised classes.
		:return:
		"""
		self.clog.debug('Setting inv A Matrix')

		self._Ainv = np.linalg.inv(self.A)

		return 0


	@property
	def R(self):
		"""
		Get complete R matrix
		:return:
		"""

		if self._R is None: self._make_R()

		return self._R
	@property
	def RTan(self):
		"""
		Get RTan matrix
		:return:
		"""

		if self._R is None: self._make_R()

		return self._R[:, 1:, :]
	@property
	def RT(self):
		"""
		Get  the transpose of the complete R matrix
		:return:
		"""

		if self._R is None: self._make_R()

		return self._R.transpose((0,2,1))
	@property
	def RTanT(self):
		"""
		Get the transpose of the RTan matrix
		:return:
		"""

		if self._R is None: self._make_R()

		return self._R[:, 1:, :].transpose((0,2,1))

	@property
	def A(self):
		"""
		Get the A=RTanT x RTan matrix
		:return:
		"""

		if self._A is None: self._make_A()

		return self._A

	@property
	def Ainv(self):
		"""
		Get the inverse of A=RTanT x RTan matrix
		:return:
		"""

		if self._Ainv is None: self._make_Ainv()

		return self._Ainv



	@property
	def Amean(self):
		"""
		Return the mean value of the matrix A
		:return:
		"""

		if self._A is None: self._make_A()

		return np.mean(self._A,axis=0)

	@property
	def Ainvmean(self):
		"""
		Return the mean value of the inverse matrix A
		:return:
		"""

		if self._Ainv is None: self._make_Ainv()

		return np.mean(self._Ainv,axis=0)

	def __str__(self):

		info=str(self.R)

		return info
class RMatrixSpherical(RMatrixCore):

	def __init__(self, l, b, theta, phi, degree=True, frame_orientation='left'):
		"""
		Class to define the R matrix to pass from Galactic Spherical polars to Galactic observed properties.
		(Vlos, Vl, Vb).T = R dot (Vr, Vtheta, Vphi)
		:param l: Sky l coords
		:param b:  Sky b coords
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param degree: If True angles are in degrees
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		"""

		super().__init__(l, b, theta, phi, degree=degree, frame_orientation=frame_orientation)
		self.clog = logger(self.__class__.__name__)


	def _make_Rs_matrix(self):
		"""
		Matrix to pass from Galactic Spherical to the (right oriented) Sun-Cartesian system, (V_x_sun, V_y_sun, V_z_sun) = Rs (Vr, Vtheta, Vphi).T
		:return: Rs matrix
		"""


		theta = self.theta
		phi = self.phi
		N = self.N

		ct = np.cos(theta)
		st = np.sin(theta)
		cf = np.cos(phi)
		sf = np.sin(phi)



		mat = np.zeros(shape=(N, 3, 3))
		mat[:, 0, 0] = +ct * cf * self.x_axis_sign
		mat[:, 0, 1] = -st * cf * self.x_axis_sign
		mat[:, 0, 2] = -sf * self.x_axis_sign
		mat[:, 1, 0] = ct * sf
		mat[:, 1, 1] = -st * sf
		mat[:, 1, 2] = cf
		mat[:, 2, 0] = st
		mat[:, 2, 1] = ct
		#mat[:, 2, 2] = 0 #Not neeeded they are still 0

		return mat
	def _make_Rc_matrix(self):
		"""
		Matrix to pass from  (right oriented) Sun-Cartesian system to Sky-SPherical system, (V_los, V_l, V_b) = Rs (V_x_sun, V_y_sun, V_z_sun).T
		:return:  Rc matrix
		"""

		l = self.l
		b = self.b
		N = self.N

		cl = np.cos(l)
		sl = np.sin(l)
		cb = np.cos(b)
		sb = np.sin(b)

		mat = np.zeros(shape=(N, 3, 3))
		mat[:, 0, 0] = cb * cl
		mat[:, 0, 1] = cb * sl
		mat[:, 0, 2] = sb
		mat[:, 1, 0] = -sl
		mat[:, 1, 1] = cl
		#mat[:, 1, 2] = 0 #Not neeeded they are still 0
		mat[:, 2, 0] = -sb * cl
		mat[:, 2, 1] = -sb * sl
		mat[:, 2, 2] = cb

		return mat
	def _make_R(self):
		"""
		Make R matrix, R= Rc dot Rs.
		(Vlos, Vl, Vb) = R dot (Vr, Vtheta, Vphi).T
		:return: 0
		"""

		self.clog.debug('Setting R Matrix')
		Rs = self._make_Rs_matrix()
		Rc = self._make_Rc_matrix()
		self._R = np.matmul(Rc, Rs)
		self.clog.debug('Rs\n: %s'%Rs.__str__())
		self.clog.debug('Rc\n: %s'%Rc.__str__())
		self.clog.debug('R\n: %s'%self._R.__str__())

		return 0
class RMatrixCylindrical(RMatrixCore):

	def __init__(self, l, b, theta, phi, degree=True, frame_orientation='left'):
		"""
		Class to define the R matrix to pass from Galactic Cylindical  to Galactic observed properties.
		(Vlos, Vl, Vb).T = R dot (VR, Vz, Vphi)
		:param l: Sky l coords
		:param b:  Sky b coords
		:param theta:  Galactic zenithal angle (asin(z/r)), Not needed by we use this in the parameters to mantain   the same formalism
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param degree: If True angles are in degrees
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		"""

		super().__init__(l, b, theta, phi, degree=degree, frame_orientation=frame_orientation)
		self.clog = logger(self.__class__.__name__)

	def _make_Rs_matrix(self):
		"""
		Matrix to pass from Galactic Cylindrical to the (right oriented) Sun-Cartesian system, (V_x_sun, V_y_sun, V_z_sun) = Rs (VR, Vz, Vphi).T
		:return: Rs matrix
		"""

		phi = self.phi
		N = self.N


		cf = np.cos(phi)
		sf = np.sin(phi)



		mat = np.zeros(shape=(N, 3, 3))
		mat[:, 0, 0] = cf * self.x_axis_sign
		#mat[:, 0, 1] =  0 * self.x_axis_sign #Not neeeded they are still 0
		mat[:, 0, 2] = -sf* self.x_axis_sign
		mat[:, 1, 0] = sf
		#mat[:, 1, 1] =  0 #Not neeeded they are still 0
		mat[:, 1, 2] = cf
		#mat[:, 2, 0] = 0 #Not neeeded they are still 0
		mat[:, 2, 1] = 1
		#mat[:, 2, 2] = 0 #Not neeeded they are still 0

		return mat
	def _make_Rc_matrix(self):
		"""
		Matrix to pass from  (right oriented) Sun-Cartesian system to Sky-SPherical system, (V_los, V_l, V_b) = Rs (V_x_sun, V_y_sun, V_z_sun).T
		:return:  Rc matrix
		"""

		l = self.l
		b = self.b
		N = self.N

		cl = np.cos(l)
		sl = np.sin(l)
		cb = np.cos(b)
		sb = np.sin(b)

		mat = np.zeros(shape=(N, 3, 3))
		mat[:, 0, 0] = cb * cl
		mat[:, 0, 1] = cb * sl
		mat[:, 0, 2] = sb
		mat[:, 1, 0] = -sl
		mat[:, 1, 1] = cl
		#mat[:, 1, 2] = 0 #Not neeeded they are still 0
		mat[:, 2, 0] = -sb * cl
		mat[:, 2, 1] = -sb * sl
		mat[:, 2, 2] = cb

		return mat
	def _make_R(self):
		"""
		Make R matrix, R= Rc dot Rs.
		(Vlos, Vl, Vb) = R dot (VR, Vz, Vphi).T
		:return: 0
		"""

		self.clog.debug('Setting R Matrix')
		Rs = self._make_Rs_matrix()
		Rc = self._make_Rc_matrix()
		self._R = np.matmul(Rc, Rs)
		self.clog.debug('Rs\n: %s'%Rs.__str__())
		self.clog.debug('Rc\n: %s'%Rc.__str__())
		self.clog.debug('R\n: %s'%self._R.__str__())

		return 0
class CovMatrix:

	def __init__(self, Sr, Stheta, Sphi, C_r_theta=0, C_r_phi=0, C_theta_phi=0):
		"""
		Make and return the Covariance matrix for a given model in Galactocentric spherical coordinates
		:param Sr:  Vdisp along radial direction
		:param Stheta:  Vdisp along zenithal direction (theta=asin(z/r))
		:param Sphi:  Vdisp along azimuthal direction (Phi=atan(y/x))
		:param C_r_theta:  correlation coefficent between r and theta
		:param C_r_phi:  correlation coefficent between r and phi
		:param C_theta_phi: correlation coefficent between theta and phi
		"""

		self.clog = logger(self.__class__.__name__)
		self.Ncomp = 1
		self.Vdisp = (Sr, Stheta, Sphi)
		self.Vcorr = (C_r_theta, C_r_phi, C_theta_phi)
		self._Sigma = None
		self._SigmaInv = None
		self._SigmaDet = None
		self._SigmaL   = None
		self._make_Sigma_matrix()

	@property
	def Sigma(self):
		"""
		Return the covariance matrix
		:return:
		"""

		if self._Sigma is None: self._make_Sigma_matrix()

		return self._Sigma

	@property
	def SigmaT(self):
		"""
		Return the transpose covariance matrix
		:return:
		"""

		if self._Sigma is None: self._make_Sigma_matrix()

		return self._Sigma.transpose((0, 2, 1))

	@property
	def SigmaInv(self):
		"""
		Return the inverse of the covariance matrix
		:return:
		"""

		if self._SigmaInv is None: self._calc_Sigma_inv()

		return self._SigmaInv

	@property
	def SigmaInvT(self):
		"""
		Return the transpose of  inverse of the covariance matrix
		:return:
		"""

		if self._SigmaInv is None: self._cal_Sigma_inv()

		return self._SigmaInv.transpose((0, 2, 1))

	@property
	def SigmaDet(self):
		"""
		Return the determinant of the correlation matrix
		:return:
		"""

		if self._SigmaDet is None: self._calc_Sigma_det()

		return self._SigmaDet

	@property
	def SigmaL(self):
		"""
		Return the Cholesky  Matrix
		:return:
		"""

		if self._SigmaL is None: self._calc_Cholesky()

		return self._SigmaL

	@property
	def SigmaLT(self):
		"""
		Return the transpose of the Cholesky  Matrix
		:return:
		"""

		return self.SigmaL.transpose((0, 2, 1))


	def _make_Sigma_matrix(self):
		"""
		Make the covariance matrix
		:return:
		"""

		self.clog.debug('Sigma matrix creation')

		Sr, Stheta, Sphi = self.Vdisp
		C_r_theta, C_r_phi, C_theta_phi = self.Vcorr

		_Sigma = np.zeros(shape=(self.Ncomp, 3, 3))
		_Sigma[:, 0, 0] = Sr * Sr
		_Sigma[:, 1, 1] = Stheta * Stheta
		_Sigma[:, 2, 2] = Sphi * Sphi
		_Sigma[:, 0, 1] = _Sigma[:, 1, 0] = C_r_theta * Sr * Stheta
		_Sigma[:, 0, 2] = _Sigma[:, 2, 0] = C_r_phi * Sr * Sphi
		_Sigma[:, 1, 2] = _Sigma[:, 2, 1] = C_theta_phi * Stheta * Sphi

		self._Sigma = _Sigma

		return 0

	def _calc_Sigma_inv(self):
		"""
		Make the inverse  covariance matrix
		:return:
		"""

		self.clog.debug('Sigma matrix inv creation')

		if self._Sigma is None: self._make_Sigma_matrix()

		self._SigmaInv = np.linalg.inv(self._Sigma)

		return 0

	def _calc_Sigma_det(self):

		self.clog.debug('Sigma matrix determinat estimate')

		if self._Sigma is None: self._make_Sigma_matrix()

		self._SigmaDet = np.linalg.det(self._Sigma)

		return 0

	def _calc_Cholesky(self):

		self.clog.debug('Sigma matrix Cholesky decomposition')
		if self._Sigma is None: self._make_Sigma_matrix()

		self._SigmaL = np.linalg.cholesky(self._Sigma)

		return 0

	def __str__(self):

		cov_matrix=self.Sigma[0]
		info = "\n Covariance Matrix"
		info += "\n|%11.3f, %11.3f, %11.3f|" % (cov_matrix[0, 0], cov_matrix[0, 1], cov_matrix[0, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (cov_matrix[1, 0], cov_matrix[1, 1], cov_matrix[1, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (cov_matrix[2, 0], cov_matrix[2, 1], cov_matrix[2, 2])

		return info

class Observed:

	def __init__(self, l, b, theta, phi, Vl, Vb, Vl_err, Vb_err, C_Vl_Vb, R=None, z=None, R_err=None, z_err=None, r=None, r_err=None, frame_orientation='left'):
		"""
		A class to contain and manage the Observable quantities
		:param l: Galactic sky longitude [deg]
		:param b: Galactic sky latitude [deg]
		:param theta:  Galactic latitude [deg]
		:param phi:  Galactic lonmgitude [deg]
		:param Vl:  Velocity along l (already corrected for the Sun Motion) [kms/s]
		:param Vb:  Velocity along b (already corrected for the Sun Motion) [kms/s]
		:param Vl_err:  Velocity error along l  [kms/s]
		:param Vb_err: Velocity error along b  [kms/s]
		:param C_Vl_Vb:  correlation coefficent Vl_Vb_error
		:param R: Galactic cylindrical radius
		:param z: Galactic cylindrical height
		:param R_err: error on Galactic cylindrical radius
		:param z_err: error on Galactic cylindrical height
		"""

		self.clog=logger(self.__class__.__name__)
		self.N                           =  len(l)
		self.l                           =  np.atleast_1d(l)
		self.b                           =  np.atleast_1d(b)
		self.theta                       =  np.atleast_1d(theta)
		self.phi                         =  np.atleast_1d(phi)
		self.Vl                          =  np.atleast_1d(Vl)
		self.Vb                          =  np.atleast_1d(Vb)
		self.Vl_err                      =  np.atleast_1d(Vl_err)
		self.Vb_err                      =  np.atleast_1d(Vb_err)
		self.C_Vl_vb                     =  np.atleast_1d(C_Vl_Vb)
		self.frame_orientation 			 = frame_orientation
		self._VskyTan          			 =  None
		self._ErrMatrix                  =  None
		self._RMatrixSpherical           =  None
		self._RMatrixCylindrical         =  None
		self.R 				             =  None
		self.z 				             =  None
		self.r							 =  None
		self.R_err 			             =  None
		self.z_err 			             =  None
		self.r_err						 =  None


		if R is not None:          self.R  = np.atleast_1d(R)
		if z is not None:          self.z  = np.atleast_1d(z)
		if r is not None:		   self.r  = np.atleast_1d(r)
		if R_err is not None:  self.R_err  = np.atleast_1d(R_err)
		if z_err is not None:  self.z_err  = np.atleast_1d(z_err)
		if r_err is not None:  self.r_err  = np.atleast_1d(r_err)

	@property
	def VskyTan(self):
		"""
		:return:  THe vector containing the Vl and Vb info (dim Nx2x1)
		"""

		if self._VskyTan is None: self._make_VskyTan()

		return self._VskyTan
	@property
	def ErrMatrix(self):

		if self._ErrMatrix is None: self._make_Err_matrix()

		return self._ErrMatrix
	@property
	def RMatrixSpherical(self):

		if self._RMatrixSpherical is None: self._make_R_matrix_spherical()

		return self._RMatrixSpherical
	@property
	def RMatrixCylindrical(self):

		if self._RMatrixCylindrical is None: self._make_R_matrix_cylindrical()

		return self._RMatrixCylindrical
	@property
	def CovMatrix(self):
		"""
		:return: The Covariance Marix (Nx2x2)
		"""

		if self._ErrMatrix is None: self._make_Err_matrix()

		return self._ErrMatrix.Error

	def _make_Err_matrix(self):
		"""
		Set the Covariance Matrix object.
		:return: 0
		"""

		eM = ErrorMatrix(self.Vl_err, self.Vb_err, self.C_Vl_vb)

		self._ErrMatrix = eM

		return 0
	def _make_R_matrix_spherical(self):
		"""
		Set the Rotation Matrix object (from Spherical to observed)
		:return: 0
		"""

		RM = RMatrixSpherical(self.l, self.b, self.theta, self.phi, degree=True, frame_orientation=self.frame_orientation)

		self._RMatrixSpherical = RM

		return 0
	def _make_R_matrix_cylindrical(self):
		"""
		Set the Rotation Matrix object (from Cylindrical to observed)
		:return: 0
		"""

		RM = RMatrixCylindrical(self.l, self.b, self.theta, self.phi, degree=True, frame_orientation=self.frame_orientation)

		self._RMatrixCylindrical = RM

		return 0
	def _make_VskyTan(self):
		"""
		Set the VskyTan vecotr
		:return:  0
		"""

		self._VskyTan = np.vstack((self.Vl, self.Vb)).T.reshape(self.N,2,1)

	def __str__(self):
		info='Class Observed, Number of objects: %i'%self.N
		return info

class ErrorMatrix:

	def __init__(self, Vl_err, Vb_err, C_Vl_Vb):
		"""
		Make the Covariance matrix for the observable velocity
		:param Vl_err: 1-sigma error for V along l  (in km/s)
		:param Vb_err: 1-sigma error for V along b  (in km/s)
		:param C_Vl_Vb: Correlation coeff between Vl_err and Vb_err
		"""

		self.clog=logger(self.__class__.__name__)


		self.Vl_err = np.atleast_1d(Vl_err)
		self.Vb_err = np.atleast_1d(Vb_err)
		self.C_Vl_Vb = np.atleast_1d(C_Vl_Vb)
		self.N = len(Vl_err)
		self._Error=None
		self._ErrorInv=None

	@property
	def Error(self):
		"""
		Return the covariance error matrix (Npointx2x2)
		:return:
		"""

		if self._Error is None: self._make_Error_matrix()

		return self._Error

	@property
	def ErrorT(self):
		"""
		Return the transpose of the covaraince error matrix (Npointx2x2)
		:return:
		"""

		if self._Error is None: self._make_Error_matrix()

		return self._Error.transpose((0,2,1))


	@property
	def ErrorInv(self):
		"""
		Return the inverse of the covariance error matrix
		:return:
		"""


		if self._ErrorInv is None: self._make_Error_inv()

		return self._ErrorInv


	@property
	def ErrorInvT(self):
		"""
		Return the transpose of the inverse of the covariance error matrix
		:return:
		"""

		if self._ErrorInv is None: self._make_Error_inv()

		return self._ErrorInv.transpose((0,2,1))



	def _make_Error_matrix(self):
		"""
		Make the covariance error matrix (Nstar X 2 X 2)
		:return:
		"""


		self.clog.debug('Error matrix creation')

		S_obs = np.zeros(shape=(self.N, 2, 2))
		S_obs[:, 0, 0] = self.Vl_err * self.Vl_err
		S_obs[:, 1, 1] = self.Vb_err * self.Vb_err
		S_obs[:, 0, 1] = S_obs[:, 1, 0] = self.C_Vl_Vb * self.Vl_err * self.Vb_err

		self._Error = S_obs

		return  0

	def _make_Error_inv(self):
		"""
		Estimate the inverse of the covariance error matrix (Nstar X 2 X 2)
		:return:
		"""

		self.clog.debug('Error Inv creation')


		if self._Error is None: self._make_Error_matrix()

		self._ErrorInv = np.linalg.inv(self._Error)

		return  0


if __name__=="__main__":

	'''
	from GAstro.rand import consistent_skypoint_cartesian  as cgen

	Rsun=8.13
	zsun=0
	Vlsr=238
	U=11.1
	V=12.24
	W=7.25

	#Select a point in (lfh) GC coordinate
	x=np.random.uniform(-30,30)
	y=np.random.uniform(-30,30)
	z=np.random.uniform(-30,30)
	vx=np.random.uniform(-200,200)
	vy=np.random.uniform(-200,200)
	vz=np.random.uniform(-200,200)

	import astropy.units as u
	import astropy.coordinates as coord
	import gala.coordinates as gala
	from GAstro.constant import _K

	#CartesianRepresentation
	#CartesianRepresentation

	vu=u.km/u.s
	Vsun = coord.CartesianDifferential(d_x=U*vu, d_y=(V+Vlsr)*vu, d_z=W*vu)
	c = coord.Galactocentric(x=-x*u.kpc, y=y*u.kpc, z=z*u.kpc, v_x=((-vx))*vu, v_y=(vy)*vu, v_z=(vz)*vu, z_sun=zsun*u.kpc, galcen_distance=Rsun*u.kpc, galcen_v_sun=Vsun)

	sc=c.transform_to(coord.ICRS)
	gc=c.transform_to(coord.Galactic)
	gc_frame = coord.Galactocentric(galcen_v_sun=Vsun, z_sun=0*u.kpc,  galcen_distance=Rsun*u.kpc)
	sccorr=gala.reflex_correct(sc,gc_frame)
	gccorr=gala.reflex_correct(gc,gc_frame)

	ra, dec, l, b = sc.ra.value, sc.dec.value, gc.l.value, gc.b.value
	pmra, pmdec = sc.pm_ra_cosdec.value, sc.pm_dec.value
	dhelio = sc.distance.value

	Vlosc=gccorr.radial_velocity
	Vlc=gccorr.pm_l_cosb.value*dhelio*_K
	Vbc=gccorr.pm_b.value * dhelio * _K



	c.representation='cylindrical'
	VR, Vz, Vphi  = c.d_rho.value, c.d_z.value, c.d_phi / u.rad * c.rho
	Vphi=Vphi.value
	c.representation ='spherical'
	Vr, Vtheta, Vphi  = c.radial_velocity.value, c.pm_lat.value*_K*c.distance.value,  c.pm_lon.value*_K*c.distance.value*np.cos(np.radians(c.lat.value))
	theta, phi = c.lat.value, c.lon.value
	print(theta, phi)


	print('Original (Vr, Vl, Vb)',Vlosc,Vlc, Vbc)
	RS=RMatrixSpherical(l, b, theta, phi, frame_orientation='right')
	VV=np.array([Vr, Vtheta, Vphi])
	print('From Spherical (Vr, Vl, Vb)', np.matmul(RS.R[0],VV))
	#print(np.matmul(RS.RTan[0],VV))


	RC=RMatrixCylindrical(l, b, theta, phi, frame_orientation='right')
	VV=np.array([VR, Vz, Vphi])
	print('From Cylindrical (Vr, Vl, Vb)', np.matmul(RC.R[0],VV))
	print(np.matmul(RC.RTan[0],VV))
	#print(Vlosc,Vlc, Vbc)
	'''

	'''
	c=CovMatrix(10, 100, 10, C_r_theta = 0.4, C_r_phi = 0.2, C_theta_phi = 0.1)
	print(c.SigmaL)
	print(c.SigmaLT)
	print(c.Sigma)
	print(np.matmul(c.SigmaL,c.SigmaLT))
	'''

	from GAstro.rand import  consistent_skypoint_cartesian
	l,b,theta,phi = consistent_skypoint_cartesian(2)
	Vl=np.random.uniform(-100,100,2)
	Vb=np.random.uniform(-100,100,2)
	Vl_err=np.random.uniform(0.1,10,2)
	Vb_err=np.random.uniform(0.1,10,2)
	C_Vl_Vb = np.random.uniform(-1,1,2)

	o=Observed(l, b, theta, phi, Vl, Vb, Vl_err, Vb_err, C_Vl_Vb, R=None, z=None, R_err=None, z_err=None, frame_orientation='left')

	#print(o.RMatrixSpherical)
	#print(o.RMatrixCylindrical)
