# coding=utf-8
import emcee
import corner
from multiprocessing import Pool
from GAstro.prob import make_simplex
from .matrix import Observed
from datetime import datetime
import numpy as np
import pandas as pd
import os
from GAstro.rand import Multivariate
from GAstro.plot import ploth2
import psutil

def logprob(params, fitmodel, observed, vdisp_order=None, vphi_order=None):

	lnprob= fitmodel.logprob(observed ,params, vdisp_order=vdisp_order, vphi_order=vphi_order)

	return lnprob


def fitsample(observed, fitmodel,  nwalkers=300, nburn=100, nstep=400, vdisp_order=None, vphi_order=None, nproc=1, progress=False):
	"""
	A class to contain and manage the Observable quantities
	:param(Mandatory) observed: An instance of the class poe.matrix.Observed
	:param(Mandatory) fitmodel: An instance of the class  poe.fitmodel.FitComponents
	:param(default=300) nwalkers:  Number of walkers to use in the MCMC (see emcee documentation)
	:param(default=100) nburn:  Number of MCMC steps used as burning-in (they are discarded in the final sample, see emcee documentation))
	:param(default=400) nstep:  Number of MCMC steps (see emcee documentatio)
	:param(default=None) vdisp_order:  If not None, it could be the string ascending or descending.
	If ascending the velocity disperion (all the three components) of the fitted components are forced to be in ascending order.
	:param(default=None) vphi_order:  Same as vdisp_order but for the Vphi parameter.
	:param(default=1) nproc:  Number of processor to use in the MCMC exploration (see emcee documentation)
	:param(default=False) progress: It true show the progress bar during MCMC exploration (see emcee documentation)
	:return An instance of the class poe.fitutility.MCMC_output
	"""
	Ntotal=nburn+nstep

	ndim = fitmodel.Nparam

	#Generate_prior
	p0 = fitmodel.sample_from_prior(nwalkers)

	process = psutil.Process(os.getpid())
	#print('Process p0 definition', process.pid, process.memory_info().rss/1e6)  # in bytes

	if nproc==1:
		sampler = emcee.EnsembleSampler(nwalkers, ndim, logprob, args=[fitmodel, observed],kwargs={'vdisp_order':vdisp_order,'vphi_order':vphi_order})
		#print('Process creation sampler', process.pid, process.memory_info().rss / 1e6)  # in bytes

		sampler.run_mcmc(p0, Ntotal, progress=progress)
	else:
		with Pool(nproc) as pool:
			#Notice: this is slower than the serial code, because the estimate of the likelihood is fast enough (~1e-5 s per evaluation)
			#that the overhead due do the fork procedure is dominating.
			sampler = emcee.EnsembleSampler(nwalkers, ndim, logprob, args=[fitmodel, observed],kwargs={'vdisp_order':vdisp_order,'vphi_order':vphi_order}, pool=pool)
			sampler.run_mcmc(p0, Ntotal, progress=progress)

	process = psutil.Process(os.getpid())
	#print('Process p0 exit fit', process.pid, process.memory_info().rss/1e6)  # in bytes

	output = MCMC_Output(sampler, fitmodel, discard=nburn, observed=observed, thin=1)

	#print('Process p0 creation  output', process.pid, process.memory_info().rss/1e6)  # in bytes

	return output


class MCMC_Output:

	def __init__(self, sampler, fitmodel, discard=0, observed=None, thin=1, time=None):

		self.thin =thin
		self._sampler=sampler
		self.creation_time = datetime.now()
		self.Nobject = None
		self.discard = discard
		self.fitmodel = fitmodel
		self._samples_tab = None
		self.time = time
		self._q_particle_table = None
		self.observed = None

		if observed is not None:
			if isinstance(observed, Observed)==False: raise ValueError('Obs in Input is not a valid isinstance of class Observed')
			self.Nsample = observed.N
			self.observed = observed


	def chain(self,i):

		return self._sampler.get_chain(discard=self.discard, thin=self.thin)[:,i,:]

	@property
	def param_names(self):

		if self.fitmodel.Ncomponents==1: ret=self.fitmodel.param_names[1:]
		else:  ret=self.fitmodel.param_names[:]

		return ret

	@property
	def dimension(self):

		if self.fitmodel.Ncomponents==1: ret=self._sampler.ndim-1
		else: ret =self._sampler.ndim

		return ret

	@property
	def samples(self):

		samples = np.copy(self._sampler.get_chain(discard=self.discard, thin=self.thin, flat=True))
		samples[:, :self.fitmodel.Ncomponents] = make_simplex(samples[:, :self.fitmodel.Ncomponents])

		if self.fitmodel.Ncomponents==1: samples=samples[:,1:]


		return samples

	@property
	def logprob(self):

		return self._sampler.get_log_prob(discard=self.discard, thin=self.thin, flat=True)

	@property
	def MAP(self):
		idxmax = np.argmax(self.logprob)
		max_param = self.samples[idxmax]
		max_log = self.logprob[idxmax]

		return max_log, max_param

	@property
	def BIC(self):

		if self.Nsample is None: raise ValueError('Unknown Nsample')

		maxL, max_param = self.MAP
		dimension = self.dimension

		BIC = np.log(self.Nsample)*dimension - 2*maxL

		return BIC, max_param

	@property
	def Qp(self):

		Q, _ = self._sampler.get_blobs(discard=self.discard, thin=self.thin, flat=True).T
		Q = np.dstack(Q) #NOTE: the dimension of Q are NxMXL where N is the number of fitted object, M is the number of compoenets and L is the number of step

		return Q


	@property
	def beta(self):

		_, beta_from_blob = self._sampler.get_blobs(discard=self.discard, thin=self.thin, flat=True).T
		ret = np.vstack(beta_from_blob)
		return ret


	@property
	def acceptance_fraction(self):

		acc = self._sampler.acceptance_fraction

		return np.mean(acc), np.std(acc)

	@property
	def autocorrelation(self):

		return self._sampler.get_autocorr_time(discard=self.discard, quiet=True)

	@property
	def MAP_kinematic_model(self):

		map_param = self.MAP[-1]
		if self.fitmodel.Ncomponents==1: map_param = np.r_[1,map_param]
		#NB return model is made in the way that it automatically eliminate the first elements of the array
		#that are the weights. When we use only one component, the only weight is removed from the list of
		#param in this class, so we have to artificially put it into the param list

		return self.fitmodel.return_models(map_param)

	@property
	def median_kinematic_model(self):

		median_param = self.percentile(q=0.5).values[:-1] #-1 Because the last param is the beta (see TODO in percentile)
		if self.fitmodel.Ncomponents==1: median_param = np.r_[1,median_param]
		#NB return model is made in the way that it automatically eliminate the first elements of the array
		#that are the weights. When we use only one component, the only weight is removed from the list of
		#param in this class, so we have to artificially put it into the param list

		return self.fitmodel.return_models(median_param)

	def percentile(self, q=(0.025, 0.16, 0.50, 0.84, 0.975)):

		sample_pd = self.sample_tab().filter(regex="^(?!(beta|LogProb|idx_chain)$).*$") #TODO: in this way I am not removing beta

		return sample_pd.quantile(q)


	@property
	def q_particle_table(self):
		"""
		Generate the table (pandas Dataframe) containing the statistic of the q parameters rapresenting the probability that
		a given particle belong to a given component.
		:return: a pandas Data frame with column  l, b (if observed is not None), qi_MAP, qi_50, qi_16, qi_84 where i indicate the component.
		"""

		if self._q_particle_table is None:

			percent_table = np.percentile(self.Qp, q=(50, 16, 84), axis=-1).transpose([1, 2, 0]).reshape(-1, self.fitmodel.Ncomponents*3)

			idxmax = np.argmax(self.logprob)
			MAP_table = self.Qp[:,:,idxmax]

			dic_cols = {}
			if self.observed is not None:
				dic_cols['l'] = self.observed.l
				dic_cols['b'] = self.observed.b
			for i in range(self.fitmodel.Ncomponents):
				dic_cols['q%i_MAP'%(i+1)]   = MAP_table[:,i]
				dic_cols['q%i_50'%(i+1)]    = percent_table[:,i*3+0]
				dic_cols['q%i_16'%(i+1)]    = percent_table[:,i*3+1]
				dic_cols['q%i_84'%(i+1)]    = percent_table[:,i*3+2]

			self._q_particle_table = pd.DataFrame(dic_cols)


		return self._q_particle_table

	def save_q_particle_table(self,file):

		self.q_particle_table.to_csv(file, index=False)

		return file


	def sample_tab(self, buf=None, file=None, add_info=True):
		"""
		Return the sampled params in a tabled format with the last two columns that are the logprob and the chain that estimated the
		set of parameters
		:param buf: buf object where to write the table
		:param file: file object where to write the table
		:return: a Pandas dataframe
		"""

		nwalkers = self._sampler.nwalkers
		nstep = self._sampler.iteration - self.discard

		if self._samples_tab is None:
			idx_chain = np.atleast_2d(np.arange(nwalkers)+1) #idx of a single chain
			idx_chain = np.repeat(idx_chain, nstep,axis=0).flatten()
			samples   = self.samples
			logprob   = self.logprob
			Col_name  = list(self.param_names) + ['beta_%i'%(i+1) for i in range(self.fitmodel.Ncomponents)] + ['LogProb', 'idx_chain']

			dic_cols  = {}
			for i in range(self.dimension):
				dic_cols[Col_name[i]] = samples[:,i]
			for i  in range(self.fitmodel.Ncomponents): dic_cols['beta_%i'%(i+1)] =self.beta[:,i]

			dic_cols[Col_name[-2]] = logprob
			dic_cols[Col_name[-1]] = idx_chain

			self._samples_tab = pd.DataFrame(dic_cols)



		if (buf is not None) or (file is not None):
			_str =  self._samples_tab.to_string(col_space=15, float_format='%4f', index=False, justify='center')

		if buf is not None:
			if add_info: buf.write(self.__str__())
			buf.write('#'+_str)
		if file is not None:
			with open(file,'w') as buf:
				if add_info: buf.write(self.__str__())
				buf.write('#'+_str)

		return self._samples_tab

	def generate_VskyTan_from_MAP(self, Npersample=1, random_seed=None):


		if self.observed is None: raise ValueError('The observed matrix is needed to use generate_VskyTan_from_MAP')
		else:

			obs=self.observed
			MAP_models = self.MAP_kinematic_model
			if len(MAP_models)==1:
				VskyTan = self.MAP_kinematic_model[0].generate_skyTan(obs.l, obs.b, obs.theta, obs.phi, Npersample=Npersample, cov_matrix_list=obs.CovMatrix, frame_orientation=obs.frame_orientation, random_seed=random_seed)
				return VskyTan
			else:
				#DEFINE ALL THE q_MAP
				q_array = np.array( [ self.q_particle_table['q%i_MAP' % (i + 1)] for i in range(self.fitmodel.Ncomponents)] ) #TODO: can do better
				VskyTan = []
				#ITERATE over particles
				for i in range(obs.N):
					#ITERATE over models
					p = []
					for j,model in enumerate(MAP_models):
						if j==0: VskyTan_choice =  model.generate_skyTan(obs.l[i], obs.b[i], obs.theta[i], obs.phi[i], Npersample=Npersample,cov_matrix_list=np.atleast_3d(obs.CovMatrix[i]).T,frame_orientation=obs.frame_orientation, random_seed=random_seed).T
						else: VskyTan_choice = np.r_[VskyTan_choice, model.generate_skyTan(obs.l[i], obs.b[i], obs.theta[i], obs.phi[i], Npersample=Npersample,cov_matrix_list=np.atleast_3d(obs.CovMatrix[i]).T,frame_orientation=obs.frame_orientation, random_seed=random_seed).T]
						p += [q_array[j,i], ] * Npersample

					idx_sample = np.random.choice( np.arange(len(VskyTan_choice))   , Npersample, p=p/np.nansum(p) )
					VskyTan += list( np.array(VskyTan_choice)[idx_sample] )
				VskyTan = np.array(VskyTan)

		return VskyTan.T #Vl, Vb (2xM)

	def generate_VskyTan_from_median(self, Npersample=1, random_seed=None):


		if self.observed is None: raise ValueError('The observed matrix is needed to use generate_VskyTan_from_MAP')
		else:

			obs=self.observed
			MAP_models = self.median_kinematic_model
			if len(MAP_models)==1:
				VskyTan = self.median_kinematic_model[0].generate_skyTan(obs.l, obs.b, obs.theta, obs.phi, Npersample=Npersample, cov_matrix_list=obs.CovMatrix, frame_orientation=obs.frame_orientation, random_seed=random_seed)
				return VskyTan
			else:
				#DEFINE ALL THE q_MAP
				q_array = np.array( [ self.q_particle_table['q%i_50' % (i + 1)] for i in range(self.fitmodel.Ncomponents)] ) #TODO: can do better
				VskyTan = []
				#ITERATE over particles
				for i in range(obs.N):
					#ITERATE over models
					p = []
					for j,model in enumerate(MAP_models):
						if j==0: VskyTan_choice =  model.generate_skyTan(obs.l[i], obs.b[i], obs.theta[i], obs.phi[i], Npersample=Npersample,cov_matrix_list=np.atleast_3d(obs.CovMatrix[i]).T,frame_orientation=obs.frame_orientation, random_seed=random_seed).T
						else: VskyTan_choice = np.r_[VskyTan_choice, model.generate_skyTan(obs.l[i], obs.b[i], obs.theta[i], obs.phi[i], Npersample=Npersample,cov_matrix_list=np.atleast_3d(obs.CovMatrix[i]).T,frame_orientation=obs.frame_orientation, random_seed=random_seed).T]
						p += [q_array[j,i], ] * Npersample

					idx_sample = np.random.choice( np.arange(len(VskyTan_choice))   , Npersample, p=p/np.nansum(p) )
					VskyTan += list( np.array(VskyTan_choice)[idx_sample] )
				VskyTan = np.array(VskyTan)

		return VskyTan.T #Vl, Vb (2xM)


	def __str__(self):
		date_time = self.creation_time.strftime("%m/%d/%Y, %H:%M:%S")
		h='# MCMC INFO\n'
		#CHAIN INFO
		h+='# Date: %s \n'%date_time
		if self.time is None: h+='# Fitted Time: Unkown \n'
		else: h+='# Elapsed Time: %.3f s (%.3f min, %.3f h) \n'%(self.time, self.time/60, self.time/3600)
		h+='# Nwalkers=%i, Nstep=%i, Nburn=%i, Ntotal=%i, thin=%i \n'%(self._sampler.nwalkers, self._sampler.iteration-self.discard, self.discard, self._sampler.iteration, self.thin)
		#PARAM INFO
		'''
		h+='# FIXED PARAM: '
		for pnames in self.Option.fixed_param:
			if pnames  in self.Option.param_names: pass
			else:
				h+='%s=%.3f, '%(pnames,self.Option.fixed_param[pnames])
		h=h[:-2]
		h+='\n'
		h+='# FITTED PARAM: %s \n'%str(self.Option.param_names)[1:-1]
		'''
		#ACCEPTANCE FRACTION
		acc_frac = self.acceptance_fraction
		h+='# ACCEPTANCE FRACTION: %.3f +- %.3f\n'%(acc_frac[0],acc_frac[1])
		#AUTOCORR
		param_autocorr = self.autocorrelation
		mean_autocorr = np.mean(param_autocorr)
		h+='# AUTOCORRELATION (nstep/val !!should be larger than 50 to believe it!!): mean=%.3f (%.3f) \n# '%(mean_autocorr, (self._sampler.iteration-self.discard)/mean_autocorr)
		for pnames,value in zip(self.fitmodel.param_names,param_autocorr):
			h+='%s=%.3f (%.3f), '%(pnames, value, (self._sampler.iteration-self.discard)/value)
		h=h[:-2]
		h+='\n'
		#MAP
		logmap, map = self.MAP
		bic,_ = self.BIC
		h+='# MAP: \n# '
		for pnames,value in zip(self.fitmodel.param_names,map):
			h+='%s=%.3f, '%(pnames, value)
		h+='LOGPROB= %.3f (BIC= %.3f)\n'%(logmap,bic)
		#PERCENTILE
		percentile = self.percentile()
		h+='# POSTERIOR \n'
		for pnames in self.param_names:

			med       = percentile.loc[0.5, pnames]
			sigma_up  = percentile.loc[0.84, pnames] - med
			sigma_low = med - percentile.loc[0.16, pnames]
			sigma2_up = percentile.loc[0.975, pnames] - med
			sigma2_low = med - percentile.loc[0.025, pnames]

			h+='# %s=%.3f (+%.3f -%.3f, 1sigma) (+%.3f -%.3f, 2sigma)\n'%(pnames, med, sigma_up, sigma_low, sigma2_up,sigma2_low)
		for i in range(self.fitmodel.Ncomponents):

			name='beta_%i'%(i+1)
			med       = percentile.loc[0.5, name]
			sigma_up  = percentile.loc[0.84, name] - med
			sigma_low = med - percentile.loc[0.16, name]
			sigma2_up = percentile.loc[0.975, name] - med
			sigma2_low = med - percentile.loc[0.025, name]
			h+='# %s=%.3f (+%.3f -%.3f, 1sigma) (+%.3f -%.3f, 2sigma)\n'%(name, med, sigma_up, sigma_low, sigma2_up,sigma2_low)


		return h


	def print_log(self, buf=None, file=None, kin_models=True):

		_str =  self.__str__()
		if kin_models:
			map_models       = self.MAP_kinematic_model
			kinematic_models = self.median_kinematic_model
			_str  += '\n KINEMATIC MODELS: MAP \n'
			for i in range(self.fitmodel.Ncomponents):
				_str  +='---------MODEL %i----------\n'%i
				_str  += map_models[i].__str__()+'\n'
			_str  += '\n KINEMATIC MODELS: MEDIAN \n'
			for i in range(self.fitmodel.Ncomponents):
				_str  +='---------MODEL %i----------\n'%i
				_str  += kinematic_models[i].__str__()+'\n'


		if buf is not None:
			buf.write(_str)
		if file is not None:
			with open(file,'w') as buf:
				buf.write(_str)

		return _str

	def save(self, outdir='.', folder_name='MCMC_data', add_info=True, kin_models=True, sample_tab=False, q_particle_tab=True):

		if folder_name is None: outfolder = outdir
		else: outfolder = outdir+'/'+folder_name
		if not os.path.exists(outfolder): os.makedirs(outfolder)

		if sample_tab: self.sample_tab(file=outfolder+'/sample.dat', add_info=add_info)
		if q_particle_tab: self.save_q_particle_table(file=outfolder+'/q_particle.csv')
		self.print_log(file=outfolder+'/MCMC_info.log', kin_models=kin_models)



		return outfolder



	def __getitem__(self, item):

		return self.chain(item)



import matplotlib.pyplot as plt
import corner
from matplotlib.gridspec import GridSpec
from matplotlib.ticker import FormatStrFormatter
from emcee.autocorr import function_1d as autocorr_func
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib
matplotlib.use('Agg')

class MCMC_Plot:

	def __init__(self, Output, labels_fontsize=18, ticks_fontsize=18):

		#Check if the input are ok
		if isinstance(Output, MCMC_Output)==False: raise ValueError('Output in Input is not a valid isinstance of class MCMC_Output')

		self.Output = Output
		self._organize_schedule={1: (1,1), 2:(1,2), 3:(1,3), 4:(2,2), 5:(2,3), 6:(2,3),
								 7: (3,3), 8:(3,3), 9: (3,3),10:(3,4), 11:(3,4), 12: (3,4),
								 13: (4,4), 14:(4,4), 15:(4,4), 16: (4,4)}
		self.labels_fontsize = labels_fontsize
		self.ticks_fontsize  = ticks_fontsize

	def corner_plot(self, save=None, color='C2', show_titles=True, quantiles=(0.16,0.50,0.84), truths='MAP', truth_color='C3', plot_datapoints=False, plot_density=False, fill_contours=True, prior=True,  **kwargs):

		labels = self.Output.param_names
		samples = self.Output.samples
		if truths.upper()=='MAP': truths=self.Output.MAP[-1]

		if prior and self.Output.fitmodel.Ncomponents==1: p0 = self.Output.fitmodel.sample_from_prior(10000)[:,1:] #Generate priors excluding the weight is only one component is present
		elif prior: p0 = self.Output.fitmodel.sample_from_prior(10000) #Generate priors
		else: p0 = None


		fig = corner.corner(samples, truths=truths, truth_color=truth_color, color=color, show_titles=show_titles, quantiles=quantiles, labels=labels,
							plot_datapoints=plot_datapoints, plot_density=plot_density, fill_contours=fill_contours,
							hist_kwargs={'lw':2}, prior=p0, prior_color='blue', **kwargs)

		if save is  not None:
			fig.savefig(save)

		return fig

	def trace_plot(self, save=None, row_prior=True, figsize=None, thin=1, labels_fontsize=None, ticks_fontsize=None, **kwargs):

		Nparam   = self.Output.dimension
		gs = self._plot_grid(Nparam, row_prior=row_prior)
		samples = self.Output.samples[0:-1:thin]
		labels = self.Output.param_names

		grid_option = self._grid_option(Nparam, row_prior)
		if figsize is None: figsize=(5*grid_option[1],5*grid_option[0])
		fig = plt.figure(figsize=figsize)
		if labels_fontsize is None: labels_fontsize = self.labels_fontsize
		if ticks_fontsize is None: ticks_fontsize = self.ticks_fontsize

		for gsi, sample, name in zip(gs,samples.T,labels):
			ax = fig.add_subplot(gsi)
			ax.plot(sample,**kwargs)
			ax.set_xlabel('$N$',fontsize=labels_fontsize)
			ax.set_ylabel(name,fontsize=labels_fontsize)
			ax.set_xticks([0,len(samples)])
			ax.xaxis.set_major_formatter(FormatStrFormatter('%.2e'))
			ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)

		gs.tight_layout(fig)
		if save is  not None:
			fig.savefig(save)

		return fig

	def autocorr_plot(self, save=None, Nchains=10, row_prior=True, figsize=None, labels_fontsize=None, ticks_fontsize=None, **kwargs):

		Nparam   = self.Output.dimension
		original_chains = self.Output._sampler.iteration-self.Output.discard
		if Nchains>original_chains: Nchains = original_chains
		choose_list = np.arange(0,Nchains)
		idx_chains = np.random.choice(choose_list, Nchains, replace=False)

		chosen_chains = self.Output[idx_chains].transpose((1,2,0))

		labels = self.Output.param_names
		gs = self._plot_grid(Nparam, row_prior=row_prior)
		grid_option = self._grid_option(Nparam, row_prior=row_prior)
		if figsize is None: figsize=(5*grid_option[1],5*grid_option[0])
		fig = plt.figure(figsize=figsize)
		if labels_fontsize is None: labels_fontsize = self.labels_fontsize
		if ticks_fontsize is None: ticks_fontsize = self.ticks_fontsize

		autocorrs = self.Output.autocorrelation

		#Since we are going more than once in one ax, we define all the axes before to enter in the cycle
		#to avoid to redifine again them.
		axl = [fig.add_subplot(gsi) for gsi in gs]

		#SINGLE CHAINS
		for chain in chosen_chains:
			for ax, sample, name in zip(axl, chain, labels):
				autocorr_sample = autocorr_func(sample)
				ax.plot(autocorr_sample, **kwargs)
				ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)



		averaged_chain = np.median(self.Output[:],axis=1).T
		#AVERAGE
		for ax, sample, name, autocorr in zip(axl, averaged_chain, labels, autocorrs):
			autocorr_sample = autocorr_func(sample)
			ax.plot(autocorr_sample, lw=3, c='k')
			ax.set_xlabel('$Lag$', fontsize=labels_fontsize)
			ax.set_ylabel('$\\rho$ %s' % name, fontsize=labels_fontsize)
			ax.xaxis.set_major_formatter(FormatStrFormatter('%i'))
			ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
			ax.axvline(autocorr, c='k', ls='dashed', lw=2)
			Lag_limit = 50*autocorr
			ax.axvspan(0, Lag_limit, alpha=0.1, color='gray')


		gs.tight_layout(fig)
		if save is  not None:
			fig.savefig(save)

		return fig

	def plot_likelihood(self,  save=None, figsize=None, labels_fontsize=None, ticks_fontsize=None, **kwargs):

		if figsize is None: figsize=(8,8)
		fig, ax =  plt.subplots(1,1, figsize=figsize)
		if labels_fontsize is None: labels_fontsize = self.labels_fontsize
		if ticks_fontsize is None: ticks_fontsize = self.ticks_fontsize

		#LIKELIHOOD
		min = np.percentile(self.Output.logprob[np.isfinite(self.Output.logprob)], q=0.01)
		MAP = self.Output.MAP[0]
		ax.hist(self.Output.logprob, range=(min,MAP),bins=30,histtype='step',color='C2',lw=3,**kwargs)
		ax.axvline(MAP, c='red')
		ax.set_xlabel('LogProb', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize, rotation=45)

		fig.tight_layout()
		if save is not None: fig.savefig(save)

		return fig

	def plot_beta(self, save=None, row_prior=True, figsize=None, labels_fontsize=None, ticks_fontsize=None,**kwargs):

		beta_list = self.Output.beta.T
		grid_option = self._grid_option(len(beta_list), row_prior=row_prior)
		gs = self._plot_grid(len(beta_list), row_prior=row_prior)
		if figsize is None: figsize=(5*grid_option[1],5*grid_option[0])
		fig = plt.figure(figsize=figsize)
		if labels_fontsize is None: labels_fontsize = self.labels_fontsize
		if ticks_fontsize is None: ticks_fontsize = self.ticks_fontsize
		idxmax = np.argmax(self.Output.logprob)

		i=0
		for gsi, beta in zip(gs,beta_list):
			ax = fig.add_subplot(gsi)
			ax.hist(beta,range=(-3,1),bins=30,histtype='step',color='C2',lw=3,**kwargs)
			ax.axvline(beta[idxmax],c='red')
			ax.set_xlabel('$\\beta_%i$'%(i+1),fontsize=labels_fontsize)
			ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
			i+=1

		fig.tight_layout()
		if save is not None: fig.savefig(save)


		return fig

	def plot_all(self, outdir='.', folder_name='MCMC_images', Obs=None, figsize=None, row_prior=True, thin=1, labels_fontsize=None, ticks_fontsize=None, cornes_kwargs={}, trace_kwargs={}, autocorr_kwargs={}, sample_kwargs={}, logprob_kwargs={}):

		if folder_name is None: outfolder = outdir
		else: outfolder = outdir+'/'+folder_name
		if not os.path.exists(outfolder): os.makedirs(outfolder)

		try: self.corner_plot(save=outfolder+'/corner.png', **cornes_kwargs)
		except: pass
		#self.corner_plot(save=outfolder + '/corner.png', **cornes_kwargs)
		#self.beta_plot(save=outfolder+'/beta_corner.png', **cornes_kwargs)
		try: self.trace_plot(save=outfolder+'/trace.png',figsize=figsize, row_prior=row_prior, thin=thin, labels_fontsize=labels_fontsize, ticks_fontsize=ticks_fontsize, **trace_kwargs)
		except: pass
		try: self.autocorr_plot(save=outfolder+'/autocorr.png',figsize=figsize, row_prior=row_prior, labels_fontsize=labels_fontsize, ticks_fontsize=ticks_fontsize, **autocorr_kwargs)
		except: pass
		try:	self.plot_likelihood(save=outfolder+'/logprob.png', labels_fontsize=labels_fontsize, ticks_fontsize=ticks_fontsize, **logprob_kwargs)
		except: pass
		try: self.plot_beta(save=outfolder+'/beta.png',labels_fontsize=labels_fontsize, ticks_fontsize=ticks_fontsize)
		except: pass
		if Obs is not None:
			try: self.sample_summary(Obs,save=outfolder+'/sample_summary.png', figsize=figsize, row_prior=row_prior, labels_fontsize=labels_fontsize, ticks_fontsize=ticks_fontsize, **sample_kwargs)
			except: pass
			#self.sample_summary(Obs,save=outfolder+'/sample_summary.png', figsize=figsize, row_prior=row_prior, labels_fontsize=labels_fontsize, ticks_fontsize=ticks_fontsize, **sample_kwargs)

		return outfolder

	#TODO better residual plot?
	def sample_summary(self, Obs, save=None, row_prior=True, figsize=None, labels_fontsize=None, ticks_fontsize=None, **kwargs):
		"""
		Make a plot with the position and the kinematic of the analysed stars.

		NB. Il plot 2D dei residui, presentava un problema ossia il fatto che il campione che osserviamo e solo un campionamento
		della distribuzione originale. Se dal modello estraiamo lo stesso numero di oggetti, questo effetto di campionamento di amplifica.
		Abbiamo deciso pertanto di campionare dal modell con circa 1000 oggetti per ogni singola stella osservata. Dividendo l'istogramma
		finale per 1000 abbiamo il numero medio di oggetti del modello riducendo l effetto di campionamento.  Il problema e che nelle zone dove non ci sono
		i dati il valore S/N tende ad essere molto alto, per cui per rendere il confronto dati-modelli piu chiaro mettiamo a nan tutti i pixel
		dove non ci sono i dati.

		:param Obs:
		:param save:
		:param row_prior:
		:param figsize:
		:param labels_fontsize:
		:param ticks_fontsize:
		:param kwargs:
		:return:
		"""

		Npersample = 10

		if isinstance(Obs, Observed)==False: raise ValueError('Output in Input is not a valid isinstance of class Observed')

		gs = self._plot_grid(9, row_prior=row_prior)
		grid_option = self._grid_option(9, row_prior=row_prior)
		if figsize is None: figsize=(5*grid_option[1],5*grid_option[0])
		fig = plt.figure(figsize=figsize)
		if labels_fontsize is None: labels_fontsize = self.labels_fontsize
		if ticks_fontsize is None: ticks_fontsize = self.ticks_fontsize


		#Make qtable
		tab_q_MAP = self.Output.q_particle_table['q1_MAP'].values
		#tab_q_50 = self.Output.q_particle_table['q1_50'].values
		Vlr, Vbr = Multivariate(np.vstack((Obs.Vl, Obs.Vb)).T, Obs.CovMatrix  , Npersample=Npersample, flatten=True)
		#Hdata, Vledge, Vbedge = np.histogram2d(Vlr, Vbr, bins=40, range=((-600, 600), (-600, 600)))
		#Hdata=np.where(np.isnan(Hdata),0,Hdata)
		Hdata, Vledge, Vbedge = np.histogram2d(Obs.Vl, Obs.Vb, bins=20, range=((-600, 600), (-600, 600)))
		Hdata = np.where(np.isnan(Hdata), 0, Hdata)
		err_data2 = Hdata[:]


		#l-b
		ax = fig.add_subplot(gs[0])
		l=np.where(Obs.l>180, 360-Obs.l, Obs.l)
		ax.scatter(l, Obs.b, marker='o', c=tab_q_MAP, zorder=10000, cmap='cool',edgecolors='k')
		ax.set_xlabel('$\ell$ [deg]', fontsize=labels_fontsize)
		ax.set_ylabel('$b$ [deg]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(180,-180)
		ax.set_ylim(-90,90)




		#R-z
		ax = fig.add_subplot(gs[1])
		if (Obs.R is not None) and (Obs.z is not None):
			ax.errorbar(Obs.R, Obs.z, Obs.R_err, Obs.z_err, fmt='.', capsize=3, ecolor='k')
			ax.scatter(Obs.R, Obs.z, marker='o', c=tab_q_MAP, zorder=10000, cmap='cool',edgecolors='k')
			ax.set_xlabel('$R_\\mathrm{cyl}$ [kpc]', fontsize=labels_fontsize)
			ax.set_ylabel('$z$ [kpc]', fontsize=labels_fontsize)
			ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)



		# Vl-Vb Model MAP
		ax = fig.add_subplot(gs[3])
		ax.set_title('MAP model', fontsize=labels_fontsize)
		ax.errorbar(Obs.Vl, Obs.Vb, xerr=Obs.Vl_err, yerr=Obs.Vb_err, fmt='.', capsize=3, ecolor='k')
		ax.scatter(Obs.Vl, Obs.Vb, marker='o', c=tab_q_MAP, zorder=10000, cmap='cool',edgecolors='k')

		Vl_map, Vb_map = self.Output.generate_VskyTan_from_MAP( Npersample=Npersample, random_seed=None)

		H_map, edges_map, _ = ploth2(Vl_map, Vb_map, ax=ax, range=((-600,600),(-600,600)), cmap='gray_r',bins=20)
		err_map2 = H_map / (Npersample*Npersample)
		H_map = np.where(np.isnan(H_map), 0, H_map / Npersample)

		#Hmapsum = np.nansum(H_map)
		#err_map2 =  H_map/(Hmapsum*Hmapsum) + (H_map*H_map/(Hmapsum*Hmapsum*Hmapsum))
		#H_map=np.where(np.isnan(H_map),0,H_map/Hmapsum)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)

		ax = fig.add_subplot(gs[4])
		ax.set_title('Obs-Teo/Obs', fontsize=labels_fontsize)
		map_diff = np.where( Hdata==0, np.nan, (Hdata-H_map)/(np.sqrt(err_data2+err_map2)))

		img=ax.imshow(map_diff.T, origin='lower', extent=[Vledge[0],Vledge[-1],Vbedge[0],Vbedge[-1]],  cmap='PuOr',vmin=-3, vmax=3)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)
		divider = make_axes_locatable(ax)
		cax = divider.append_axes("right", size="5%", pad=0.)
		cbar = plt.colorbar(img, cax=cax)
		cbar.solids.set_edgecolor("face")



		# Vl-Vb Model Kinematic
		ax = fig.add_subplot(gs[6])
		ax.set_title('Kinematic model', fontsize=labels_fontsize)
		ax.errorbar(Obs.Vl, Obs.Vb, xerr=Obs.Vl_err, yerr=Obs.Vb_err, fmt='.', capsize=3, ecolor='k')
		ax.scatter(Obs.Vl, Obs.Vb, marker='o', c=tab_q_MAP, zorder=10000, cmap='cool',edgecolors='k')

		Vl_median, Vb_median = self.Output.generate_VskyTan_from_median( Npersample=1000, random_seed=None)
		H_median, edges_median, _ = ploth2(Vl_median, Vb_median, ax=ax, range=((-600,600),(-600,600)), cmap='gray_r',bins=20)
		err_median2 = H_median / (Npersample*Npersample)
		H_median = np.where(np.isnan(H_median), 0, H_median / Npersample)


		#Hmediansum = np.nansum(H_median)
		#err_median2 =  H_median/(Hmediansum *Hmediansum ) + (H_map*H_map/(Hmediansum *Hmediansum *Hmediansum ))
		#H_median=np.where(np.isnan(H_median),0,H_median/Hmediansum)

		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)

		ax = fig.add_subplot(gs[7])
		ax.set_title('Obs-Teo/Obs', fontsize=labels_fontsize)
		map_diff = np.where( (Hdata==0), np.nan, (Hdata-H_median)/((np.sqrt(err_data2+err_median2))))
		img=ax.imshow(map_diff.T, origin='lower', extent=[Vledge[0],Vledge[-1],Vbedge[0],Vbedge[-1]],  cmap='PuOr',vmin=-3, vmax=3)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)
		divider = make_axes_locatable(ax)
		cax = divider.append_axes("right", size="5%", pad=0.)
		cbar = plt.colorbar(img, cax=cax)
		cbar.solids.set_edgecolor("face")



		#Vl dist
		ax = fig.add_subplot(gs[5])
		ax.hist([Obs.Vl, Vl_map, Vl_median], range=(-300,300), bins=15, lw=3, histtype='step',density=True)
		ax.hist([Vlr,], range=(-300,300), bins=15, lw=3, histtype='step',density=True, ls='dashed',color='k',zorder=1000)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)

		#Vb dist
		ax = fig.add_subplot(gs[8])
		ax.hist([Obs.Vb, Vb_map, Vb_median], range=(-300,300), bins=15, lw=3, histtype='step',density=True)
		ax.hist([Vbr,], range=(-300,300), bins=15, lw=3, histtype='step',density=True, ls='dashed',color='k',zorder=1000)
		ax.set_xlabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)



		'''
		#Generate_more_data
		Vlr, Vbr = Multivariate(np.vstack((Obs.Vl, Obs.Vb)).T, Obs.CovMatrix  , Npersample=1000, flatten=True)
		Hdata, Vledge, Vbedge = np.histogram2d(Vlr, Vbr, bins=40, range=((-600, 600), (-600, 600)))
		Hdata=np.where(np.isnan(Hdata),0,Hdata)


		#Vl-Vb Model MAP
		ax = fig.add_subplot(gs[3])
		ax.set_title('MAP model', fontsize=labels_fontsize)
		Kmap = self.Output.MAP_kinematic_model
		Vl_map, Vb_map = Kmap.generate_sky(Obs.l, Obs.b, Obs.theta, Obs.phi, cov_matrix_list=Obs.CovMatrix, Npersample=1000, degree=True, frame_orientation='left', random_seed=None)
		#ax.scatter(Vl_map, Vb_map,c='C1',s=0.01)
		ax.errorbar(Obs.Vl, Obs.Vb, xerr=Obs.Vl_err, yerr=Obs.Vb_err, fmt='o', capsize=3)
		H_map, edges_map, _ = ploth2(Vl_map, Vb_map, ax=ax, range=((-600,600),(-600,600)), cmap='gray_r',bins=40)
		H_map=np.where(np.isnan(H_map),0,H_map)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)



		#
		ax = fig.add_subplot(gs[4])
		ax.set_title('Obs-Teo/Obs', fontsize=labels_fontsize)
		map_diff = (Hdata-H_map)/(Hdata)
		img=ax.imshow(map_diff.T, origin='lower', extent=[Vledge[0],Vledge[-1],Vbedge[0],Vbedge[-1]],  cmap='PuOr',vmin=-3, vmax=3)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)
		divider = make_axes_locatable(ax)
		cax = divider.append_axes("right", size="5%", pad=0.)
		cbar = plt.colorbar(img, cax=cax)
		cbar.solids.set_edgecolor("face")

		print(np.nansum(Hdata-H_map))
		input()

		ax = fig.add_subplot(gs[6])#Vl-Vb Model median

		ax.set_title('Median model', fontsize=labels_fontsize)
		Kmap = self.Output.median_kinematic_model
		Vl_med, Vb_med = Kmap.generate_sky(Obs.l, Obs.b, Obs.theta, Obs.phi, cov_matrix_list=Obs.CovMatrix, Npersample=1000, degree=True, frame_orientation='left', random_seed=None)
		ax.errorbar(Obs.Vl, Obs.Vb, xerr=Obs.Vl_err, yerr=Obs.Vb_err, fmt='o', capsize=3)
		ploth2(Vl_med, Vb_med, ax=ax, range=((-600,600),(-600,600)), cmap='gray_r',bins=40)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)

		#gg
		ax = fig.add_subplot(gs[7])
		map_diff = (Hdata-H_map)/(Hdata)
		ax.set_title('Obs-Teo/Obs', fontsize=labels_fontsize)
		ax.imshow(map_diff.T, origin='lower', extent=[Vledge[0],Vledge[-1],Vbedge[0],Vbedge[-1]],  cmap='PuOr',vmin=-3, vmax=3)
		ax.set_ylabel('$V_{b}$ [km/s]', fontsize=labels_fontsize)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		ax.set_xlim(-600,600)
		ax.set_ylim(-600,600)
		divider = make_axes_locatable(ax)
		cax = divider.append_axes("right", size="5%", pad=0.)
		cbar = plt.colorbar(img, cax=cax)
		cbar.solids.set_edgecolor("face")


		#Vl dist
		ax = fig.add_subplot(gs[5])
		ax.hist([Obs.Vl, Vl_map, Vl_med], range=(-300,300), bins=15, lw=3, histtype='step',density=True)
		ax.hist([Vlr,], range=(-300,300), bins=15, lw=3, histtype='step',density=True, ls='dashed',color='k',zorder=1000)
		ax.set_xlabel('$V_{\ell}$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)


		#Vb dist
		ax = fig.add_subplot(gs[8])
		ax.hist([Obs.Vb, Vb_map, Vb_med], range=(-300,300), bins=15, lw=3, histtype='step', density=True)
		ax.hist([Vbr,], range=(-300,300), bins=15, lw=3, histtype='step',density=True, ls='dashed',color='k',zorder=1000)
		ax.set_xlabel('$V_b$ [km/s]', fontsize=labels_fontsize)
		ax.tick_params(axis='both', which='major', labelsize=ticks_fontsize)
		'''


		gs.tight_layout(fig)
		if save is  not None:
			fig.savefig(save)

		return fig

	def _plot_grid_subplots(self, Npanels, row_prior=True, figsize=None):
		"""
		If row prior=True use the tuple as defined in self._organize schedule, otherwise in the opposite verse (swap row
		and columns)
		:param row_prior:
		:return: the fig and the ax of the subplots
		"""
		if row_prior: grid_option = self._organize_schedule[Npanels]
		else: grid_option = self._organize_schedule[Npanels[::-1]]
		if figsize is None: figsize=(5*grid_option[1],5*grid_option[0])
		return  plt.subplots(*grid_option,figsize=figsize)

	def _plot_grid(self, Npanels, row_prior=True):
		"""
		If row prior=True use the tuple as defined in self._organize schedule, otherwise in the opposite verse (swap row
		and columns)
		:param row_prior:
		:return: the fig and the ax of the subplots
		"""

		grid_option = self._grid_option(Npanels, row_prior)
		return  GridSpec(*grid_option)

	def _grid_option(self, Npanels, row_prior=True):


		if row_prior: grid_option = self._organize_schedule[Npanels]
		else: grid_option = self._organize_schedule[Npanels[::-1]]

		return grid_option
