import numpy as np
from math import exp, log
from scipy.stats import cauchy, truncnorm

#NB: Some of the distribution are not normalised, this is not a problem when we use these distribution just as prior or
#to generate sample. In any case there is a property for each distribution that tell us if the pdf is normalised or not.
#TODO: ADD a lower and upper limit for all the distributions
class distribution:

	def __init__(self):

		self._is_normalised = None

	def logprob(self,x):

		return self._logprob(x)

	def _logprob(self,x):

		raise ValueError('Logprob not implemented for this distribution')

	def generate(self,N):

		return self._generate(N)

	def _generate(self, N):
		return ValueError('Generate not implemented for this distribution')

	@property
	def is_normalised(self):

		return self._is_normalised

class Uniform(distribution):

	def __init__(self,low_lim, up_lim):

		self.low_lim=low_lim
		self.up_lim=up_lim
		self._is_normalised = False


	def _logprob(self,x):

		if (x>self.low_lim) and (x<self.up_lim): return 0 #Nor normalised

		return -np.inf

	def _generate(self, N):

		y=np.random.uniform(self.low_lim, self.up_lim, N)

		return y


class Gaussian(distribution):

	def __init__(self, mean, std):

		self.mean=mean
		self.std=std
		self._is_normalised = False


	def _logprob(self,x):

		ex = (x-self.mean)/self.std

		return -0.5*ex*ex #Not normalised

	def _generate(self, N):

		y=np.random.normal(self.mean, self.std, N)

		return y

class PositiveGaussian(Gaussian):

	def _logprob(self,x):

		if x<0: return -np.inf

		return super()._logprob(x)

	def _generate(self, N):

		y=np.abs(super()._generate(N))

		return y

class TruncatedGaussian(Gaussian):

	def __init__(self, mean, std, low_lim=None, up_lim=None):

		super(TruncatedGaussian, self).__init__(mean, std)
		if low_lim is None: self.low_lim = -np.inf
		else: self.low_lim = low_lim
		if up_lim is None: self.up_lim = np.inf
		else:  self.up_lim = up_lim

		self._low_lim, self._up_lim = (self.low_lim - self.mean) / self.std, (self.up_lim - self.mean) / self.std
		#_low_lim and _up_lim are the quantities we give to scipy truncnorm since they are defined for the normalised
		#gaussian.  see https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.truncnorm.html
		self._is_normalised = False

	def _logprob(self,x):

		if (x<self.low_lim) or (x>self.up_lim): return -np.inf

		ex = (x-self.mean)/self.std

		return -0.5*ex*ex #Not normalised

	def _generate(self, N):


		y = truncnorm.rvs(a=self._low_lim, b=self._up_lim, loc=self.mean, scale=self.std, size=N)

		return y

class LogNormal(distribution):

	def __init__(self, mean, std):

		self.mean = mean
		self.std = std
		self._is_normalised = False


	def _logprob(self,x):

		if x<0: return -np.inf

		lx=log(x)
		ex = (lx-self.mean)/self.std

		return -0.5*ex*ex -lx #Not normalised

	def _generate(self, N):

		y=np.random.lognormal(self.mean, self.std, N)

		return y


class Cauchy(distribution):

	def __init__(self, mu, sigma):

		self.mu = mu
		self.sigma = sigma
		self._is_normalised = False


	def _logprob(self,x):

		ex = (x-self.mu)/self.sigma

		return -log(1+ex*ex) #Not normalised

	def _generate(self, N):

		y = cauchy.rvs(loc=self.mu, scale=self.sigma, size=N)

		return y

class PositiveCauchy(Cauchy):

	def _logprob(self, x):
		if x < 0: return -np.inf

		return super()._logprob(x)

	def _generate(self, N):

		y=np.abs(super()._generate(N))

		return y

class Beta(distribution):

	def __init__(self, alpha, beta):

		if alpha>=0: self.alpha = alpha
		else: raise ValueError('alpha in Beta function needs to be >0')
		if beta>=0: 	self.beta = beta
		else: raise ValueError('beta in Beta function needs to be >0')
		self._is_normalised = True


	def _logprob(self,x):

		if (x>0) and (x<1):
			return  (self.alpha-1)*log(x) + (self.beta-1)*log(1-x)

		return -np.inf

	def _generate(self, N):

		y=np.random.beta(self.alpha, self.beta, N)

		return y


class Dirichlet(distribution):

	def __init__(self, *alfas):

		self.alfas=np.atleast_1d(alfas)
		self._is_normalised = True


	def _logprob(self,x):

		x=np.atleast_1d(x)

		if len(alfas)!=len(x):raise ValueError("Not equal")

		return np.sum((self.alfas-1)*np.log(x))

	def _generate(self, N):

		y=np.random.dirichlet(self.alfas,N)

		return y



if __name__=="__main__":

	'''
	x=3
	y=-2
	u=Uniform(-5,5)
	print(u.logprob(y))
	u=Gaussian(0,1)
	print(u.logprob(x))
	u=PositiveGaussian(0,1)
	print(u.logprob(-x))
	u=LogNormal(0,1)
	print(u.logprob(x))
	print(u.logprob(-x))
	u=Cauchy(0,1)
	print(u.logprob(x))
	print(u.logprob(-x))
	u=PositiveCauchy(0,1)
	print(u.logprob(x))
	print(u.logprob(-x))
	u=Beta(0,1)
	print(u.logprob(0.5))
	print(u.logprob(-0.5))
	'''

	'''
	import matplotlib.pyplot as plt
	N=1000
	u=Uniform(-5,5)
	y=u.generate(N)
	plt.hist(y)
	print(y)
	plt.show()
	u=Gaussian(0,1)
	y=u.generate(N)
	plt.hist(y)
	print(y)
	plt.show()
	u=PositiveGaussian(0,1)
	y=u.generate(N)
	plt.hist(y)
	print(y)
	plt.show()
	u=LogNormal(3,1)
	y=u.generate(N)
	plt.hist(y,bins=100)
	print(y)
	plt.show()
	u=Cauchy(0,1)
	y=u.generate(N)
	plt.hist(y,bins=100)
	print(y)
	plt.show()
	u=PositiveCauchy(0,1)
	y=u.generate(N)
	plt.hist(y,bins=100)
	print(y)
	plt.show()
	u=Beta(3,1)
	y=u.generate(N)
	plt.hist(y,bins=100)
	print(y)
	plt.show()
	u=Dirichlet(1,1,1)
	y=u.generate(N)
	plt.hist(y,bins=100)
	print(y)
	'''


	import matplotlib.pyplot as plt
	tg=TruncatedGaussian(100,200,low_lim=50)
	s=tg.generate(10000)
	plt.hist(s)
	plt.show()
