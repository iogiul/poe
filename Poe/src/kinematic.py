from .matrix import CovMatrix,  RMatrixSpherical, RMatrixCylindrical, Observed
import logging
import  numpy as np
from GAstro.rand import Multivariate


def logger(className):
	return logging.getLogger(__name__+':'+className)

class KinematicModel:

	def __init__(self,  label=None):
		"""
		Super class for the kinematic Model class
		:param label:  Name of the model, if None John Doe is used as default name
		"""
		self.clog=logger(self.__class__.__name__)
		if label is None: self.label='John Doe'
		else: self.label=label
		self.clog.debug('Model with name %s created'%self.label)
		self.beta=0

class Halo(KinematicModel):

	def __init__(self, Sr, Stheta, Sphi, Vr=0, Vtheta=0, Vphi=0,  C_r_theta=0, C_r_phi=0, C_theta_phi=0, label=None):
		"""
		Define a Gaussian kinematical model in spherical coordinates.
		:param Sr:  Vdisp along radial direction
		:param Stheta:  Vdisp along zenithal direction (theta=asin(z/r))
		:param Sphi:  Vdisp along azimuthal direction (theta=atan(y/x))
		:param Vr:  Mean velocity along r
		:param Vtheta:  Mean velocity along theta
		:param Vphi:  Mean velocity along phi
		:param C_r_theta:  correlation coefficent between r and theta
		:param C_r_phi:  correlation coefficent between r and phi
		:param C_theta_phi:  correlation coefficent between theta and phi
		:param label: Name of the model
		"""

		super().__init__(label=label)
		self.clog = logger(self.__class__.__name__)

		self.clog=logger(self.__class__.__name__)
		self.Vr, self.Vtheta, self.Vphi = Vr, Vtheta, Vphi
		self.V=np.array((Vr, Vtheta, Vphi))
		self.Sr, self.Stheta,self.Sphi = Sr, Stheta, Sphi
		self.Vdisp=(Sr, Stheta, Sphi)
		self.Vcorr=(C_r_theta,C_r_phi,C_theta_phi)
		self.beta = 1 - ((self.Sphi**2)+(self.Stheta**2))/(2*self.Sr**2)
		self.CovMatrix=CovMatrix(Sr=Sr, Stheta=Stheta, Sphi=Sphi, C_r_theta=C_r_theta, C_r_phi=C_r_phi, C_theta_phi=C_theta_phi)

		self.clog.debug('Model with name %s created'%self.label)

	#Class methods
	@classmethod
	def anisotropy(cls, Sr, beta, Vr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label=None):
		"""
		Define the model from its anisotroy (in this case Stheta=Sphi)
		:param Sr: Vdisp along radial direction
		:param beta: Anisotropy parameter
		:param Vr:  Mean velocity along r
		:param Vtheta:  Mean velocity along theta
		:param Vphi:  Mean velocity along phi
		:param C_r_theta:  correlation coefficent between r and theta
		:param C_r_phi:  correlation coefficent between r and phi
		:param C_theta_phi:  correlation coefficent between theta and phi
		:param label: Name of the model
		:return:
		"""

		sigmar2 = Sr * Sr
		St = np.sqrt(sigmar2*(1-beta))

		return cls(Sr=Sr, Stheta=St, Sphi=St, Vr=Vr, Vtheta=Vtheta, Vphi=Vphi, C_r_theta=C_r_theta, C_r_phi=C_r_phi, C_theta_phi=C_theta_phi, label=label)
	#Properties
	@property
	def cov_matrix(self):

		return self.CovMatrix.Sigma[0]
	@property
	def is_model_physical(self):
		"""
		Check if the kinematic model is actually a physical model.
		A correlation matrix is positive definite, this means that the determinat should be 0.
		Therefore if the determinant is negative, a physical model described by the given covariance matrix cannot exist.
		:return: True if the determinant of the covariance matrix is positive otherwise 0.
		"""

		det = np.linalg.det(self.cov_matrix)

		return True if det>0 else False
	#Transformation
	def Vsky(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Calculate the  Vlos, Vl, Vb projection of the mean Vr, Vtheta, Vphi at a given l,b,theta,phi
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixSpherical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An array with Vlos, Vl, Vb for each point (dim Nx3x1)
		"""

		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixSpherical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixSpherical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixSpherical')

		#make Rmatrix
		Vtensor = np.atleast_2d(self.V).T
		Vsky=np.matmul(RMat.R,Vtensor)

		current_log_level = logging.getLevelName(logging.getLogger().getEffectiveLevel())
		if current_log_level == 'DEBUG':
			self.clog.debug('Dimension of R %s' % (str(RMat.R.shape)))
			self.clog.debug('Dimension of V %s'%(str(Vtensor.shape)))
			self.clog.debug('Module of V %f'%(np.sqrt(np.sum(Vtensor**2))))
			self.clog.debug('Dimension of Vsky %s'%(str(Vsky.shape)))
			self.clog.debug('Module of Vsky %s'%str(np.sqrt(np.sum(Vsky**2,axis=1))))

		return Vsky
	def VskyTan(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Calculate the   Vl, Vb projection of the mean Vr, Vtheta, Vphi at a given l,b,theta,phi
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixSpherical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An array with Vl, Vb for each point (dim Nx2x1)
		"""

		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixSpherical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixSpherical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixSpherical')

		#make Rmatrix
		Vtensor = np.atleast_2d(self.V).T
		VskyTan=np.matmul(RMat.RTan,Vtensor)

		current_log_level = logging.getLevelName(logging.getLogger().getEffectiveLevel())
		if current_log_level == 'DEBUG':
			self.clog.debug('Dimension of RTan %s' % (str(RMat.RTan.shape)))
			self.clog.debug('Dimension of V %s'%(str(Vtensor.shape)))
			self.clog.debug('Module of V %f'%(np.sqrt(np.sum(Vtensor**2))))
			self.clog.debug('Dimension of VskyTan %s'%(str(VskyTan.shape)))
			self.clog.debug('Module of VskyTan %s'%str(np.sqrt(np.sum(VskyTan**2,axis=1))))

		return VskyTan
	def SkyTanCov(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Transform the Intrinsic spherical ellipsoid to the Vl, Vb ellipsoid projected on the sky at fiven l,b, theta, phi.
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixSpherical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An numpy array Nx2x2 containing  the covariance matrix for Vl Vb for each point in input.
		"""
		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixSpherical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixSpherical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixSpherical')

		#make Rmatrix
		Lambda =   np.matmul( np.matmul(RMat.RTan,self.CovMatrix.Sigma), RMat.RTanT)
		return  Lambda
	def SkyCov(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Transform the Intrinsic spherical ellipsoid to the Vlos, Vl, Vb ellipsoid projected on the sky at fiven l,b, theta, phi.
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixSpherical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An numpy array Nx3x3 containing  the covariance matrix for Vlos, Vl, Vb for each point in input.
		"""
		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixSpherical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixSpherical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixSpherical')

		#make Rmatrix
		Lambda =   np.matmul( np.matmul(RMat.R,self.CovMatrix.Sigma), RMat.RT)
		return  Lambda
	#Sample generation
	def generate_spherical(self, N, random_seed=None):
		"""
		Draw velocities (Vr, Vtheta, Vphi) from the kinematic model
		:param N: number of tuple of 3D velocities to draw
		:return:  A 3xN array
		"""
		np.random.seed(seed=random_seed)
		sample = np.random.multivariate_normal(self.V, self.cov_matrix, N)
		return sample.T
	def generate_cartesian(self, theta, phi, Npersample=1, degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (cartesian, Vx, Vy, Vz) at a given theta, phi.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param degree: If True angles are in degrees
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: Return Vx, Vy, Vz each is a 1D numpy array with length Npersample*len(theta).
		"""

		theta = np.repeat(np.atleast_1d(theta), Npersample)
		phi = np.repeat(np.atleast_1d(phi), Npersample)
		N=len(theta)

		if degree:
			theta = np.radians(theta)
			phi = np.radians(phi)

		ct = np.cos(theta)
		st = np.sin(theta)
		cf = np.cos(phi)
		sf = np.sin(phi)

		Vr, Vtheta, Vphi = self.generate_spherical(N, random_seed=random_seed)

		Vxg = -ct*cf*Vr + st*cf*Vtheta + sf*Vphi
		Vyg =  ct*sf*Vr - st*sf*Vtheta + cf*Vphi
		Vzg =  st*Vr    + ct*Vtheta

		if frame_orientation=='right': Vxg=-Vxg

		return np.vstack((Vxg, Vyg, Vzg))
	def generate_sky(self, l, b, theta, phi, Npersample=1, degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (sky, Vlos, Vl, Vb) at a given l,b, theta, phi.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param degree: If True angles are in degrees
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(len(l)*Npersample)
		"""
		#Exploiting the fact that with R matrix we can project the covariance matrix on the sky and sample directly from there,
		np.random.seed(random_seed)
		coords=[l,b,theta,phi]
		Wm = self.Vsky(coords=coords, degree=degree, frame_orientation=frame_orientation)
		WCov = self.SkyCov(coords=coords, degree=degree, frame_orientation=frame_orientation)

		return Multivariate(Wm[:, :, 0], WCov, Npersample=Npersample, flatten=True)
	def generate_skyTan(self, l, b, theta, phi, Npersample=1, cov_matrix_list=None,  degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (Tan sky, Vl, Vb) at a given l,b, theta, phi.
		Moreover, if cov_matrix_list is not None, the final sample is dispersed using Gaussian centred in the generates sample
		and covariance matrixes given in  to cov_matrix_list. This is useful to convolve the final sample with observation uncertains.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param cov_matrix_list: List of 2x2 Vl, Vb covariance matrix. One matrix for each l-b-theta-phi element.
		:param degree: If True angles are in degrees
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(len(l)*Npersample)
		"""

		#Exploiting the fact that with R matrix we can project the covariance matrix on the sky and sample directly from there,
		np.random.seed(random_seed)
		coords=[l,b,theta,phi]
		Wm = self.VskyTan(coords=coords, degree=degree, frame_orientation=frame_orientation)
		WCov = self.SkyTanCov(coords=coords, degree=degree, frame_orientation=frame_orientation)
		SkySample=Multivariate(Wm[:, :, 0], WCov, Npersample=Npersample, flatten=True)

		if cov_matrix_list is not None:
			cov_matrix_list=np.repeat(cov_matrix_list,Npersample,axis=0)
			SkySample = Multivariate(SkySample.T, cov_matrix_list, Npersample=1, flatten=True)

		return SkySample
	#likelihood
	def likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vr, Vtheta, Vphi distribution given the kinematic model.
		Still to be implemented
		:return:
		"""

		raise NotImplementedError('Likelihood based on Vr, Vtheta, Vphi not  implemented yet')

		return 0
	def sky_likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vl, Vb, Vlos distribution given the kinematic model.
		Still to be implemented
		:return:
		"""
		raise NotImplementedError('Likelihood based on Vlos, Vl, Vb not  implemented yet')

		return 0
	def skyTan_rawlikelihood(self,observed_matrix):
		"""
		Estimate the likelihood (not log and not summed) to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:return: Return a list of length N (elements in observed_matrix) with the likelihood estimate
		"""
		if  not isinstance(observed_matrix,Observed): raise ValueError('the observed matrix needs to be an instance of the class matrix.Observed')


		#Matrix from observables
		Rot_Matrix = observed_matrix.RMatrixSpherical
		W  = observed_matrix.VskyTan

		#Model Values
		Wm = self.VskyTan(RMat=Rot_Matrix)
		Sigma_projected = self.SkyTanCov(RMat=Rot_Matrix)

		##Lambda Matrix
		#Lambda
		Lambda = Sigma_projected + observed_matrix.CovMatrix
		#LambdaInv
		LambdaInv = np.linalg.inv(Lambda)
		LambdaDet = np.linalg.det(Lambda)

		#Gau paramters
		D=(W-Wm)
		DT=D.transpose((0,2,1))

		Norm = 1 / (2* np.pi * np.sqrt(LambdaDet))

		Q= np.matmul(DT, np.matmul(LambdaInv, D))[:,0,0]

		lnlike = Norm*np.exp(-0.5*Q)

		return lnlike
	def skyTan_likelihood(self,observed_matrix):
		"""
		Estimate the likelihood to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:return:
		"""
		lnlike = self.skyTan_rawlikelihood(observed_matrix)
		lnliketot = np.sum(  np.log(lnlike)  )

		return lnliketot

	def to_cylindrical(self,theta,degree=True):
		"""
		Create a disc object starting from this model at a given angle theta. This will be a component defined in cylindrical coordinates
		:param theta:  Galactic zenithal angle (asin(z/r)). Not needed by we use this in the parameters to mantain   the same formalism
		:param degree: If True angles are in degrees
		:return: An object of the model matrix.Halo
		"""

		if degree: theta = np.radians(theta)
		ct = np.cos(theta)
		st = np.sin(theta)

		_Rot_matrix = np.array([[ct, -st, 0], [st, ct, 0], [0, 0, 1]])

		# Apply to mean
		VR, Vz, Vphi = np.matmul(_Rot_matrix, self.V)
		Cov_Spherical = np.matmul(_Rot_matrix, np.matmul(self.cov_matrix, _Rot_matrix.T))
		SR, Sz, Sphi = np.sqrt(np.diag(Cov_Spherical))
		C_R_z = Cov_Spherical[0, 1] / (SR * Sz)
		C_R_phi = Cov_Spherical[0, 2] / (SR * Sphi)
		C_z_phi = Cov_Spherical[1, 2] / (Sz * Sphi)

		return Disc(SR=SR, Sz=Sz, Sphi=Sphi, VR=VR, Vz=Vz, Vphi=Vphi, C_R_z=C_R_z, C_R_phi=C_R_phi, C_z_phi=C_z_phi)

	def __str__(self):

		info = "Kinematic component (Halo): %s" %self.label
		info += "\n<Vr>=%.3f, <Vtheta>=%.3f, <Vphi>=%.3f" %tuple(self.V)
		info += "\n sigmar=%.3f, sigmatheta=%.3f, sigmaphi=%.3f"%tuple(self.Vdisp)
		info += "\n beta=%.3f" % self.beta
		info += "\n Covariance Matrix"
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[0, 0], self.cov_matrix[0, 1], self.cov_matrix[0, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[1, 0], self.cov_matrix[1, 1], self.cov_matrix[1, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[2, 0], self.cov_matrix[2, 1], self.cov_matrix[2, 2])

		return info

class Giano(Halo):

	def __init__(self, Sr, Stheta, Sphi, Lr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label=None):
		"""
		Define a Double-Gaussian kinematical model in spherical coordinates.
		The two Gaussian are identical except for the mean value of Vr, One Gaussian peak at Vr=|Lr| and the other one at
		Vr=-|Lr|. The two Gaussians have the same relative weight.
		:param Sr:  Vdisp along radial direction
		:param Stheta:  Vdisp along zenithal direction (theta=asin(z/r))
		:param Sphi:  Vdisp along azimuthal direction (theta=atan(y/x))
		:param Lr:  The Absolute value of Vr peak of the two component
		:param Vtheta:  Mean velocity along theta
		:param Vphi:  Mean velocity along phi
		:param C_r_theta:  correlation coefficent between r and theta
		:param C_r_phi:  correlation coefficent between r and phi
		:param C_theta_phi:  correlation coefficent between theta and phi
		:param label: Name of the model
		"""

		super().__init__(Sr=Sr, Stheta=Stheta, Sphi=Sphi, Vr=0, Vtheta=Vtheta, Vphi=Vphi,  C_r_theta=C_r_theta, C_r_phi=C_r_phi, C_theta_phi=C_theta_phi, label=label)
		self.clog = logger(self.__class__.__name__)
		self.Lr_input = Lr
		self.Lr=np.abs(self.Lr_input)
		self.Sr_effective = np.sqrt(self.Sr*self.Sr + self.Lr*self.Lr)
		self.beta = 1 - ((self.Sphi**2)+(self.Stheta**2))/(2*self.Sr_effective**2)

	#Class methods
	@classmethod
	def anisotropy(cls, Sr, beta, Lr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label=None):
		"""
		Define the model from its anisotroy (in this case Stheta=Sphi). NB, that in this case
		the r component of the dipersion that goes in the anisotropy definition is not Sr, but Sr_effective=(Sr*Sr+Lr*Lr)
		:param Sr: Vdisp along radial direction
		:param beta: Anisotropy parameter
		:param Vr:  Mean velocity along r
		:param Vtheta:  Mean velocity along theta
		:param Vphi:  Mean velocity along phi
		:param C_r_theta:  correlation coefficent between r and theta
		:param C_r_phi:  correlation coefficent between r and phi
		:param C_theta_phi:  correlation coefficent between theta and phi
		:param label: Name of the model
		:return:
		"""

		sigmar2 = Sr * Sr + Lr*Lr
		St = np.sqrt(sigmar2*(1-beta))

		return cls(Sr=Sr, Stheta=St, Sphi=St, Lr=Lr, Vtheta=Vtheta, Vphi=Vphi, C_r_theta=C_r_theta, C_r_phi=C_r_phi, C_theta_phi=C_theta_phi, label=label)

	#Sample generation
	def generate_spherical(self, N, random_seed=None):
		"""
		Draw velocities (Vr, Vtheta, Vphi) from the kinematic model
		:param N: number of tuple of 3D velocities to draw
		:return:  A 3xN array
		"""
		np.random.seed(seed=random_seed)
		sample = np.random.multivariate_normal(self.V, self.cov_matrix, N)
		sample[:,0] += np.random.choice([-1,1],N)*self.Lr
		return sample.T
	def generate_cartesian(self, theta, phi, Npersample=1, degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (cartesian, Vx, Vy, Vz) at a given theta, phi.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param degree: If True angles are in degrees
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: Return Vx, Vy, Vz each is a 1D numpy array with length Npersample*len(theta).
		"""

		theta = np.repeat(np.atleast_1d(theta), Npersample)
		phi = np.repeat(np.atleast_1d(phi), Npersample)
		N=len(theta)

		if degree:
			theta = np.radians(theta)
			phi = np.radians(phi)

		ct = np.cos(theta)
		st = np.sin(theta)
		cf = np.cos(phi)
		sf = np.sin(phi)

		Vr, Vtheta, Vphi = self.generate_spherical(N, random_seed=random_seed)

		Vxg = -ct*cf*Vr + st*cf*Vtheta + sf*Vphi
		Vyg =  ct*sf*Vr - st*sf*Vtheta + cf*Vphi
		Vzg =  st*Vr    + ct*Vtheta

		if frame_orientation=='right': Vxg=-Vxg

		return np.vstack((Vxg, Vyg, Vzg))
	def generate_sky(self, l, b, theta, phi, Npersample=1, degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (sky, Vlos, Vl, Vb) at a given l,b, theta, phi.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param degree: If True angles are in degrees
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(len(l)*Npersample)
		"""

		RMat = RMatrixSpherical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		RMatR = np.repeat(RMat.R, Npersample, axis=0)
		Spherical_Sample = np.atleast_3d(self.generate_spherical(RMatR.shape[0],random_seed=random_seed).T)
		Wsky = np.matmul(RMatR,Spherical_Sample)
		return Wsky[:,:,0].T
	def generate_skyTan(self, l, b, theta, phi, Npersample=1, cov_matrix_list=None,  degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (Tan sky, Vl, Vb) at a given l,b, theta, phi.
		Moreover, if cov_matrix_list is not None, the final sample is dispersed using Gaussian centred in the generates sample
		and covariance matrixes given in  to cov_matrix_list. This is useful to convolve the final sample with observation uncertains.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param cov_matrix_list: List of 2x2 Vl, Vb covariance matrix. One matrix for each l-b-theta-phi element.
		:param degree: If True angles are in degrees
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(len(l)*Npersample)
		"""


		RMat = RMatrixSpherical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		RMatR = np.repeat(RMat.RTan, Npersample, axis=0)
		Spherical_Sample = np.atleast_3d(self.generate_spherical(RMatR.shape[0],random_seed=random_seed).T)
		Wsky = np.matmul(RMatR,Spherical_Sample)[:,:,0].T

		if cov_matrix_list is not None:
			cov_matrix_list=np.repeat(cov_matrix_list,Npersample,axis=0)
			Wsky = Multivariate(Wsky.T, cov_matrix_list, Npersample=1, flatten=True)


		return Wsky
	#likelihood
	def likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vr, Vtheta, Vphi distribution given the kinematic model.
		Still to be implemented
		:return:
		"""

		raise NotImplementedError('Likelihood based on Vr, Vtheta, Vphi not  implemented yet')

		return 0
	def sky_likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vl, Vb, Vlos distribution given the kinematic model.
		Still to be implemented
		:return:
		"""
		raise NotImplementedError('Likelihood based on Vlos, Vl, Vb not  implemented yet')

		return 0
	def skyTan_rawlikelihood(self,observed_matrix, check_Lr_input=True):
		"""
		Estimate the likelihood (not log and not summed) to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:return: Return a list of length N (elements in observed_matrix) with the likelihood estimate
		"""
		if  not isinstance(observed_matrix,Observed): raise ValueError('the observed matrix needs to be an instance of the class matrix.Observed')
		if check_Lr_input and self.Lr_input<0: return -np.inf

		#Matrix from observables
		Rot_Matrix = observed_matrix.RMatrixSpherical
		W  = observed_matrix.VskyTan


		#Model Values
		V1  = np.atleast_2d([self.Lr, self.V[1], self.V[2]]).T
		V2  = np.atleast_2d([-self.Lr, self.V[1], self.V[2]]).T
		W1m  = np.matmul(Rot_Matrix.RTan,V1)
		W2m  = np.matmul(Rot_Matrix.RTan,V2)
		Sigma_projected = self.SkyTanCov(RMat=Rot_Matrix)

		##Lambda Matrix
		#Lambda
		Lambda = Sigma_projected + observed_matrix.CovMatrix
		#LambdaInv
		LambdaInv = np.linalg.inv(Lambda)
		LambdaDet = np.linalg.det(Lambda)

		#Gau paramters
		D1=(W-W1m)
		DT1=D1.transpose((0,2,1))
		D2=(W-W2m)
		DT2=D2.transpose((0,2,1))
		Norm = 1 / (4 * np.pi * np.sqrt(LambdaDet))
		Q1 = np.matmul(DT1, np.matmul(LambdaInv, D1))[:,0,0] #To avoid to replicate 10 times the results
		Q2 = np.matmul(DT2, np.matmul(LambdaInv, D2))[:,0,0] #To avoid to replicate 10 times the results

		lnlike = Norm*( np.exp(-0.5*Q1) +  np.exp(-0.5*Q2)  )

		return lnlike
	def skyTan_likelihood(self,observed_matrix, check_Lr_input=True):
		"""
		Estimate the likelihood to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:param check_Lr_input: Since we use only the absolute value of Lr, both -Lr and +Lr in input in the creation of the model
		give the same likelihood and this can create an annoying bimodality in the parameter exploration. Setting check_Lr_input to True,
		avoid this putting a prior Lr [0,..[  in the likelihood estimate.
		:return:
		"""
		lnlike = self.skyTan_rawlikelihood(observed_matrix,check_Lr_input=check_Lr_input)
		lnliketot = np.sum(  np.log(lnlike)  )

		return lnliketot


	def __str__(self):

		info = "Kinematic component (Giano): %s" %self.label
		info += "\n<Lr>=%.3f, <Vtheta>=%.3f, <Vphi>=%.3f" %(self.Lr,self.V[1],self.V[2])
		info += "\n sigmar_effective=%.3f, sigmar=%.3f, sigmatheta=%.3f, sigmaphi=%.3f"%(self.Sr_effective,self.Vdisp[0],self.Vdisp[1],self.Vdisp[2])
		info += "\n beta=%.3f" % self.beta
		info += "\n Covariance Matrix"
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[0, 0], self.cov_matrix[0, 1], self.cov_matrix[0, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[1, 0], self.cov_matrix[1, 1], self.cov_matrix[1, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[2, 0], self.cov_matrix[2, 1], self.cov_matrix[2, 2])

		return info

class Disc(KinematicModel):

	def __init__(self, SR, Sz, Sphi, VR=0, Vz=0, Vphi=0,  C_R_z=0, C_R_phi=0, C_z_phi=0, label=None):
		"""
		Define a Gaussian kinematical model in cylindrical coordinates
		:param SR:  Vdisp along the radial direction
		:param Sz:  Vdisp along the vertical direction
		:param Sphi:  Vdisp along azimuthal direction (theta=atan(y/x))
		:param VR:  Mean velocity along R
		:param Vz:  Mean velocity along z
		:param Vphi:  Mean velocity along phi
		:param C_R_z:  correlation coefficent between R and z
		:param C_R_phi:  correlation coefficent between R and phi
		:param C_z_phi:  correlation coefficent between z and phi
		:param label: Name of the model
		"""

		super().__init__(label=label)
		self.clog = logger(self.__class__.__name__)

		self.clog=logger(self.__class__.__name__)
		self.VR, self.Vz, self.Vphi = VR, Vz, Vphi
		self.V=np.array((self.VR, self.Vz, self.Vphi))
		self.SR, self.Sz,self.Sphi = SR, Sz, Sphi
		self.Vdisp=np.array((self.SR, self.Sz,self.Sphi))
		self.Vcorr=np.array((C_R_z,C_R_phi,C_z_phi))
		#TODO: we are using the CovMatrix object that is devined for spherical coordinates. THis does not change anything practically, but at some point it is better to modify this.
		self.CovMatrix=CovMatrix(Sr=SR, Stheta=Sz, Sphi=Sphi, C_r_theta=C_R_z, C_r_phi=C_R_phi, C_theta_phi=C_z_phi)
		self.beta = 1 - ((self.Sz**2)+(self.Sphi**2))/(2*self.SR**2)

		self.clog.debug('Model with name %s created'%self.label)

	#Properties
	@property
	def cov_matrix(self):

		return self.CovMatrix.Sigma[0]
	@property
	def is_model_physical(self):
		"""
		Check if the kinematic model is actually a physical model.
		A correlation matrix is positive definite, this means that the determinat should be 0.
		Therefore if the determinant is negative, a physical model described by the given covariance matrix cannot exist.
		:return: True if the determinant of the covariance matrix is positive otherwise 0.
		"""

		det = np.linalg.det(self.cov_matrix)

		return True if det>0 else False

	#Transformation
	def Vsky(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Calculate the  Vlos, Vl, Vb projection of the mean VR, Vz, Vphi at a given l,b,theta,phi
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixCylindrical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An array with Vlos, Vl, Vb for each point (dim Nx3x1)
		"""

		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixCylindrical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixCylindrical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixCylindrical')

		#make Rmatrix
		Vtensor = np.atleast_2d(self.V).T
		Vsky=np.matmul(RMat.R,Vtensor)

		current_log_level = logging.getLevelName(logging.getLogger().getEffectiveLevel())
		if current_log_level == 'DEBUG':
			self.clog.debug('Dimension of R %s' % (str(RMat.R.shape)))
			self.clog.debug('Dimension of V %s'%(str(Vtensor.shape)))
			self.clog.debug('Module of V %f'%(np.sqrt(np.sum(Vtensor**2))))
			self.clog.debug('Dimension of Vsky %s'%(str(Vsky.shape)))
			self.clog.debug('Module of Vsky %s'%str(np.sqrt(np.sum(Vsky**2,axis=1))))

		return Vsky
	def VskyTan(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Calculate the   Vl, Vb projection of the mean VR, Vz, Vphi at a given l,b,theta,phi
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixCylindrical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An array with Vl, Vb for each point (dim Nx2x1)
		"""

		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixCylindrical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixCylindrical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixCylindrical')

		#make Rmatrix
		Vtensor = np.atleast_2d(self.V).T
		VskyTan=np.matmul(RMat.RTan,Vtensor)

		current_log_level = logging.getLevelName(logging.getLogger().getEffectiveLevel())
		if current_log_level == 'DEBUG':
			self.clog.debug('Dimension of RTan %s' % (str(RMat.RTan.shape)))
			self.clog.debug('Dimension of V %s'%(str(Vtensor.shape)))
			self.clog.debug('Module of V %f'%(np.sqrt(np.sum(Vtensor**2))))
			self.clog.debug('Dimension of VskyTan %s'%(str(VskyTan.shape)))
			self.clog.debug('Module of VskyTan %s'%str(np.sqrt(np.sum(VskyTan**2,axis=1))))

		return VskyTan
	def SkyTanCov(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Transform the Intrinsic spherical ellipsoid to the Vl, Vb ellipsoid projected on the sky at fiven l,b, theta, phi.
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RMatrixCylindrical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An numpy array Nx2x2 containing  the covariance matrix for Vl Vb for each point in input.
		"""
		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixCylindrical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixCylindrical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixCylindrical')

		#make Rmatrix
		Lambda =   np.matmul( np.matmul(RMat.RTan,self.CovMatrix.Sigma), RMat.RTanT)
		return  Lambda
	def SkyCov(self, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Transform the Intrinsic spherical ellipsoid to the Vlos, Vl, Vb ellipsoid projected on the sky at fiven l,b, theta, phi.
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RMatrixCylindrical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An numpy array Nx3x3 containing  the covariance matrix for Vlos, Vl, Vb for each point in input.
		"""
		if RMat is None:
			l, b, theta, phi = coords
			RMat=RMatrixCylindrical(l, b, theta, phi, degree, frame_orientation=frame_orientation)
		elif isinstance(RMat, RMatrixCylindrical):
			self.clog.debug('You are estimating Vsky with a given rotational matrix object')
		else:
			raise ValueError('The Rmat in input is not an instance of Poe.matrix.RMatrixCylindrical')

		#make Rmatrix
		Lambda =   np.matmul( np.matmul(RMat.R,self.CovMatrix.Sigma), RMat.RT)
		return  Lambda

	#Sample generation
	def generate_cylindrical(self, N, random_seed=None):
		"""
		Draw velocities (VR, Vz, Vphi) from the kinematic model
		:param N: number of tuple of 3D velocities to draw
		:return:  A 3xN array
		"""
		np.random.seed(seed=random_seed)
		sample = np.random.multivariate_normal(self.V, self.cov_matrix, N)
		return sample.T
	def generate_cartesian(self, theta, phi, Npersample=1, degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (cartesian, Vx, Vy, Vz) at a given theta, phi.
		:param theta:  Galactic zenithal angle (asin(z/r)). Not needed by we use this in the parameters to mantain   the same formalism
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param degree: If True angles are in degrees
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: Return Vx, Vy, Vz each is a 1D numpy array with length Npersample*len(theta).
		"""

		theta = np.repeat(np.atleast_1d(theta), Npersample)
		phi = np.repeat(np.atleast_1d(phi), Npersample)
		N=len(theta)

		if degree: phi = np.radians(phi)

		cf = np.cos(phi)
		sf = np.sin(phi)

		VR, Vz, Vphi = self.generate_cylindrical(N, random_seed=random_seed)


		Vxg = - cf*VR  +    sf*Vphi
		Vyg =   sf*VR  +    cf*Vphi
		Vzg =  Vz

		if frame_orientation=='right': Vxg=-Vxg

		return np.vstack((Vxg, Vyg, Vzg))
	def generate_sky(self, l, b, theta, phi, Npersample=1, degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (sky, Vlos, Vl, Vb) at a given l,b, theta, phi.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param degree: If True angles are in degrees
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(len(l)*Npersample)
		"""
		#Exploiting the fact that with R matrix we can project the covariance matrix on the sky and sample directly from there,
		np.random.seed(random_seed)
		coords=[l,b,theta,phi]
		Wm = self.Vsky(coords=coords, degree=degree, frame_orientation=frame_orientation)
		WCov = self.SkyCov(coords=coords, degree=degree, frame_orientation=frame_orientation)

		return Multivariate(Wm[:, :, 0], WCov, Npersample=Npersample, flatten=True)
	def generate_skyTan(self, l, b, theta, phi, Npersample=1, cov_matrix_list=None,  degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (Tan sky, Vl, Vb) at a given l,b, theta, phi.
		Moreover, if cov_matrix_list is not None, the final sample is dispersed using Gaussian centred in the generates sample
		and covariance matrixes given in  to cov_matrix_list. This is useful to convolve the final sample with observation uncertains.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param cov_matrix_list: List of 2x2 Vl, Vb covariance matrix. One matrix for each l-b-theta-phi element.
		:param degree: If True angles are in degrees
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(len(l)*Npersample)
		"""

		#Exploiting the fact that with R matrix we can project the covariance matrix on the sky and sample directly from there,
		np.random.seed(random_seed)
		coords=[l,b,theta,phi]
		Wm = self.VskyTan(coords=coords, degree=degree, frame_orientation=frame_orientation)
		WCov = self.SkyTanCov(coords=coords, degree=degree, frame_orientation=frame_orientation)
		SkySample=Multivariate(Wm[:, :, 0], WCov, Npersample=Npersample, flatten=True)

		if cov_matrix_list is not None:
			cov_matrix_list=np.repeat(cov_matrix_list,Npersample,axis=0)
			SkySample = Multivariate(SkySample.T, cov_matrix_list, Npersample=1, flatten=True)

		return SkySample

	#likelihood
	def likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vr, Vtheta, Vphi distribution given the kinematic model.
		Still to be implemented
		:return:
		"""

		raise NotImplementedError('Likelihood based on Vr, Vtheta, Vphi not  implemented yet')

		return 0
	def sky_likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vl, Vb, Vlos distribution given the kinematic model.
		Still to be implemented
		:return:
		"""
		raise NotImplementedError('Likelihood based on Vlos, Vl, Vb not  implemented yet')

		return 0
	def skyTan_rawlikelihood(self,observed_matrix):
		"""
		Estimate the likelihood (not log and not summed) to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:return: Return a list of length N (elements in observed_matrix) with the likelihood estimate
		"""
		if  not isinstance(observed_matrix,Observed): raise ValueError('the observed matrix needs to be an instance of the class matrix.Observed')


		#Matrix from observables
		Rot_Matrix = observed_matrix.RMatrixCylindrical
		W  = observed_matrix.VskyTan

		#Model Values
		Wm = self.VskyTan(RMat=Rot_Matrix)
		Sigma_projected = self.SkyTanCov(RMat=Rot_Matrix)

		##Lambda Matrix
		#Lambda
		Lambda = Sigma_projected + observed_matrix.CovMatrix
		#LambdaInv
		LambdaInv = np.linalg.inv(Lambda)
		LambdaDet = np.linalg.det(Lambda)

		#Gau paramters
		D=(W-Wm)
		DT=D.transpose((0,2,1))
		Norm = 1 / (2* np.pi * np.sqrt(LambdaDet))
		Q= np.matmul(DT, np.matmul(LambdaInv, D))[:,0,0] #To avoid to replicate 10 times the results

		lnlike = Norm*np.exp(-0.5*Q)

		return lnlike
	def skyTan_likelihood(self,observed_matrix):
		"""
		Estimate the likelihood to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:return:
		"""
		lnlike = self.skyTan_rawlikelihood(observed_matrix)
		lnliketot = np.sum(  np.log(lnlike)  )

		return lnliketot

	def to_spherical(self,theta, degree=True):
		"""
		Create an halo object starting from this model at a given angle theta. This will be a component defined in spherical coordinates
		:param theta:  Galactic zenithal angle (asin(z/r)). Not needed by we use this in the parameters to mantain   the same formalism
		:param degree: If True angles are in degrees
		:return: An object of the model matrix.Halo
		"""

		if degree: theta=np.radians(theta)
		ct=np.cos(theta)
		st=np.sin(theta)

		_Rot_matrix=np.array([[ct,st,0],[-st,ct,0],[0,0,1]])

		#Apply to mean
		Vr,Vtheta,Vphi=np.matmul(_Rot_matrix,self.V)
		Cov_Spherical = np.matmul(_Rot_matrix, np.matmul(self.cov_matrix, _Rot_matrix.T) )
		Sr, Stheta, Sphi = np.sqrt(np.diag(Cov_Spherical))
		C_r_theta = Cov_Spherical[0,1]/(Sr*Stheta)
		C_r_phi = Cov_Spherical[0,2]/(Sr*Sphi)
		C_theta_phi = Cov_Spherical[1,2]/(Stheta*Sphi)


		return Halo(Sr, Stheta, Sphi, Vr=Vr, Vtheta=Vtheta, Vphi=Vphi,  C_r_theta=C_r_theta, C_r_phi=C_r_phi, C_theta_phi=C_theta_phi, label=self.label)

	def __str__(self):

		info = "Kinematic component (Disc): %s" %self.label
		info += "\n<VR>=%.3f, <Vz>=%.3f, <Vphi>=%.3f" %tuple(self.V)
		info += "\n sigmaR=%.3f, sigmaz=%.3f, sigmaphi=%.3f"%tuple(self.Vdisp)
		info += "\n Covariance Matrix"
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[0, 0], self.cov_matrix[0, 1], self.cov_matrix[0, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[1, 0], self.cov_matrix[1, 1], self.cov_matrix[1, 2])
		info += "\n|%11.3f, %11.3f, %11.3f|" % (self.cov_matrix[2, 0], self.cov_matrix[2, 1], self.cov_matrix[2, 2])

		return info

class SkyTanBackground(KinematicModel):

	def __init__(self, Sl, Sb, Vl=0, Vb=0,  C_Vl_Vb=0, label=None):
		"""
		Define a Gaussian kinematical model directly on the space of observables.
		:param Sl:  Vdisp along Galactic l
		:param Sb:  Vdisp along Galctic b
		:param Vl:  Mean velocity along l
		:param Vb:  Mean velocity along b
		:param C_l_b:  correlation coefficent between Vl and Vb
		:param label: Name of the model
		"""

		super().__init__(label=label)
		self.clog = logger(self.__class__.__name__)

		self.clog=logger(self.__class__.__name__)
		self.Vl, self.Vb = Vl, Vb
		self.V=np.array((self.Vl, self.Vb))
		self.Sl, self.Sb  = Sl, Sb
		self.Vdisp=(self.Sl, self.Sb)
		self.Vcorr=C_Vl_Vb
		self._Sigma = None
		self._make_Sigma_matrix()
		self.beta = 1 - self.Sl**2/self.Sb**2

	@property
	def is_model_physical(self):
		"""
		Check if the kinematic model is actually a physical model.
		A correlation matrix is positive definite, this means that the determinat should be 0.
		Therefore if the determinant is negative, a physical model described by the given covariance matrix cannot exist.
		:return: True if the determinant of the covariance matrix is positive otherwise 0.
		"""

		det = np.linalg.det(self.cov_matrix)

		return True if det>0 else False
	@property
	def cov_matrix(self):

		return self._Sigma[0]

	#TODO: find a clever method to not consider the not needed parameters in generate_skyTan
	#Sample generation
	def generate_sample(self,  N=1, random_seed=None):
		"""
		Draw velocities from the kinematic model (Tan sky, Vl, Vb) at a given l,b, theta, phi.
		:param N: Number of object to draw.
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(N)
		"""

		#Exploiting the fact that with R matrix we can project the covariance matrix on the sky and sample directly from there,
		np.random.seed(random_seed)
		Wm = self.V
		WCov = self.cov_matrix
		SkySample=np.random.multivariate_normal(Wm, WCov, size=N).T

		return SkySample
	def generate_skyTan(self, l,b,theta, phi, Npersample=1, cov_matrix_list=None,  degree=True, frame_orientation='left', random_seed=None):
		"""
		Draw velocities from the kinematic model (Tan sky, Vl, Vb) at a given l,b, theta, phi.
		Moreover, if cov_matrix_list is not None, the final sample is dispersed using Gaussian centred in the generates sample
		and covariance matrixes given in  to cov_matrix_list. This is useful to convolve the final sample with observation uncertains.
		Notice that since no conversion are needed the parameters l,b,theta, phi, degree and frame_orientation are used to mantain the
		same formalism of the other kinematic model class.
		:param theta:  Galactic zenithal angle (asin(z/r))
		:param phi: Galactic azimuthal angle (atan (y/x))
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param cov_matrix_list: List of 2x2 Vl, Vb covariance matrix. One matrix for each l-b-theta-phi element.
		:param degree: If True angles are in degrees
		:param Npersample: Number of object to draw for each pair (theta, phi). The final number of objects is Npersample*len(theta)
		:param frame_orientation: if left the Galactic frame of reference is a lhs system (Sun at positive x), if right it is a rhs (Sun at negative x)
		:param random_seed: If a constant random_seed is needed.
		:return: return a 2D numpy array with dimension 3x(len(l)*Npersample)
		"""

		l=np.atleast_1d(l)
		SkySample=self.generate_sample(N=Npersample*len(l))

		if cov_matrix_list is not None:
			cov_matrix_list=np.repeat(cov_matrix_list,Npersample,axis=0)
			SkySample = Multivariate(SkySample.T, cov_matrix_list, Npersample=1, flatten=True)

		return SkySample
	#Transformation
	def VskyTan(self, N=None, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Return the Vl, Vb mean
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixSpherical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An array with Vl, Vb for each point (dim Nx2x1)
		"""
		if N is None: N=len(coords[0])

		VskyTan = np.atleast_3d(self.V)
		return np.repeat(VskyTan,N,axis=0)
	def SkyTanCov(self, N=None, coords=[], RMat=None, degree=True, frame_orientation='left'):
		"""
		Transform the Intrinsic spherical ellipsoid to the Vl, Vb ellipsoid projected on the sky at fiven l,b, theta, phi.
		:param coords: a list with l, b, theta, phi
		:param RMat: The objeted of the class RmatrixSpherical to use instead of coords, if None, use coords.
		:param degree: If True coords are in degrees, otherwise in radians.
		:return:  An numpy array Nx2x2 containing  the covariance matrix for Vl Vb for each point in input.
		"""
		if N is None: N=len(coords[0])
		Lambda=self._Sigma

		return  np.repeat(Lambda,N, axis=0)

	#likelihood
	def likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vr, Vtheta, Vphi distribution given the kinematic model.
		Still to be implemented
		:return:
		"""

		raise NotImplementedError('Likelihood based on Vr, Vtheta, Vphi not  implemented yet')

		return 0
	def sky_likelihood(self,*args):
		"""
		Estimate the likelihood to observe a given Vl, Vb, Vlos distribution given the kinematic model.
		Still to be implemented
		:return:
		"""
		raise NotImplementedError('Likelihood based on Vlos, Vl, Vb not  implemented yet')

		return 0
	def skyTan_rawlikelihood(self,observed_matrix):
		"""
		Estimate the likelihood (not log and not summed) to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:return: Return a list of length N (elements in observed_matrix) with the likelihood estimate
		"""
		if  not isinstance(observed_matrix,Observed): raise ValueError('the observed matrix needs to be an instance of the class matrix.Observed')


		#Matrix from observables
		W         = observed_matrix.VskyTan
		ErrMatrix = observed_matrix.CovMatrix

		#Model Values
		Wm = self.VskyTan(observed_matrix.N)
		Sigma_projected = self.SkyTanCov(observed_matrix.N)


		##Lambda Matrix
		#Lambda
		Lambda = Sigma_projected + ErrMatrix
		#LambdaInv
		LambdaInv = np.linalg.inv(Lambda)
		LambdaDet = np.linalg.det(Lambda)

		#Gau paramters
		D=(W-Wm)
		DT=D.transpose((0,2,1))

		Norm = 1 / (2* np.pi * np.sqrt(LambdaDet))
		#TODO: CHeck if the slice method is faster thatn .flatten or not
		Q= np.matmul(DT, np.matmul(LambdaInv, D))[:,0,0] #To avoid to replicate 10 times the results (Alternative is to use flatten)


		lnlike = Norm*np.exp(-0.5*Q)

		return lnlike
	def skyTan_likelihood(self,observed_matrix):
		"""
		Estimate the likelihood to observe a given Vl, Vb distribution given the kinematic model.
		:param observed_matrix: a matrix from the class matrix.Observed
		:return:
		"""

		lnlike = self.skyTan_rawlikelihood(observed_matrix)
		lnliketot = np.sum(  np.log(lnlike)  )

		return lnliketot

	def _make_Sigma_matrix(self):

		_Sigma = np.zeros(shape=(1, 2, 2))
		_Sigma[:, 0, 0] = self.Sl*self.Sl
		_Sigma[:, 1, 1] = self.Sb*self.Sb
		_Sigma[:, 0, 1] = _Sigma[:, 1, 0] = self.Sl*self.Sb*self.Vcorr
		self._Sigma = _Sigma

		return 0

	def __str__(self):

		info = "Kinematic component (SkyTanBackground): %s" %self.label
		info += "\n<Vl>=%.3f, <Vb>=%.3f" %tuple(self.V)
		info += "\n sigmal=%.3f, sigmab=%.3f"%tuple(self.Vdisp)
		info += "\n Covariance Matrix"
		info += "\n|%11.3f, %11.3f|" % (self.cov_matrix[0, 0], self.cov_matrix[0, 1])
		info += "\n|%11.3f, %11.3f|" % (self.cov_matrix[1, 0], self.cov_matrix[1, 1])

		return info


if __name__=='__main__':

	'''
	#Test generate samples
	if True:
		h=Halo(10, 10, 10, Vr=0, Vtheta=0, Vphi=130,  C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
		print(h)
		print(h.is_model_physical)

		from GAstro.rand import  consistent_skypoint_cartesian
		coord = consistent_skypoint_cartesian(2)
		print(h.Vsky(coord))
		print(h.VskyTan(coord))
		h2=Halo.anisotropy( 10, 0.9,Vphi=210,Vr=-100,Vtheta=0)
		print(h2)
		Vr,Vt,Vp = h2.generate_spherical(2)
		print(Vr,Vt,Vp)

		Vx,Vy,Vz = h2.generate_cartesian(coord[2],coord[3],1)
		print(Vx,Vy,Vz)

		W = h2.generate_sky(coord[0],coord[1],coord[2],coord[3],1,random_seed=10)
		print(W)
		W = h2.generate_skyTan(coord[0],coord[1],coord[2],coord[3],1,random_seed=10)
		print(W)

		print()
		coord = consistent_skypoint_cartesian(3)
		h3=Halo.anisotropy( 10, 0.6,Vphi=300,Vr=-200,Vtheta=100)
		print(h3.cov_matrix)
		print(h3.SkyCov(coords=coord, RMat=None, degree=True, frame_orientation='left'))
		print(h3.SkyTanCov(coords=coord, RMat=None, degree=True, frame_orientation='left'))

		import time
		t1=time.time()
		Wl,Wb=h3.generate_skyTan(coord[0],coord[1],coord[2],coord[3],Npersample=100000)
		t2=time.time()
		print(t2-t1)
		import matplotlib.pyplot as plt
		plt.scatter(Wl, Wb)

		t1=time.time()
		Wm = h3.VskyTan(coords=coord)
		WCov  = h3.SkyTanCov(coords=coord, RMat=None, degree=True, frame_orientation='left')
		Wl2, Wb2 = Multivariate(Wm[:,:,0], WCov,Npersample=100000, flatten=True)
		t2=time.time()
		print(t2-t1)
		plt.scatter(Wl2, Wb2)
		plt.show()
	'''

	#Test likelihood

	'''
	if True:
		h=Halo(150, 60, 60, Vr=0, Vtheta=0, Vphi=0,  C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')

		from GAstro.rand import  consistent_skypoint_cartesian
		coord = consistent_skypoint_cartesian(10)
		Wl,Wb=h.generate_skyTan(coord[0],coord[1],coord[2],coord[3],Npersample=1)
		Vl_err=np.random.uniform(0.1, 5,len(Wl))
		Vb_err=np.random.uniform(0.1, 5,len(Wl))
		C_Vl_Vb = np.zeros_like(Vl_err)
		Wl = np.random.normal(Wl, Vl_err)
		Wb = np.random.normal(Wb, Vb_err)
		obs=Observed(coord[0],coord[1],coord[2],coord[3],  Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

		values = np.linspace(-200,300,1000)
		ll=[]
		for val in values:
			htest = Halo(150, 60, 60, Vr=val, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
			ll.append(htest.skyTan_likelihood(obs))
		#ll=htest.skyTan_likelihood(obs)
		import matplotlib.pyplot as plt
		plt.plot(values,ll)
		plt.axvline(0)
		plt.show()
	'''

	#Test Giano random realisation
	'''
	if True:
		from GAstro.rand import  consistent_skypoint_cartesian
		h=Giano(130, 30, 30, Lr=160, Vtheta=0, Vphi=130,  C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
		ht=Halo(130, 30, 30, Vr=-160, Vtheta=0, Vphi=130,  C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')

		np.random.seed(101304)

		coord = consistent_skypoint_cartesian(1)
		Vlos, Vl, Vb    = h.generate_sky(coord[0],coord[1],coord[2],coord[3],Npersample=10000,random_seed=134)
		Vlos2, Vl2, Vb2 = ht.generate_sky(coord[0],coord[1],coord[2],coord[3],Npersample=10000, random_seed=134)
		Vl3, Vb3 = h.generate_skyTan(coord[0],coord[1],coord[2],coord[3],Npersample=10000,random_seed=134)

		import matplotlib.pyplot as plt
		plt.scatter(Vl,Vb)
		plt.scatter(Vl2,Vb2)
		plt.scatter(Vl3,Vb3)
		plt.show()
		#print(ht.VskyTan(coord))

		#print(h.Sr_effective, h.beta)
	'''

	#Test Giano likelihood
	'''
	if True:
		h=Giano(60, 60, 60, Lr=170, Vtheta=0, Vphi=0,  C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')

		from GAstro.rand import  consistent_skypoint_cartesian
		np.random.seed(140)
		coord = consistent_skypoint_cartesian(100)
		Wl,Wb=h.generate_skyTan(coord[0],coord[1],coord[2],coord[3],Npersample=1)
		Vl_err=np.random.uniform(0.1, 5,len(Wl))
		Vb_err=np.random.uniform(0.1, 5,len(Wl))
		C_Vl_Vb = np.zeros_like(Vl_err)
		Wl = np.random.normal(Wl, Vl_err)
		Wb = np.random.normal(Wb, Vb_err)
		obs=Observed(coord[0],coord[1],coord[2],coord[3],  Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

		values = np.linspace(-200,300,1000)
		ll=[]
		ll2=[]
		for val in values:
			htest = Halo(60, 60, 60, Vr=val, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
			htest2 = Giano(60, 60, 60, Lr=val, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
			ll.append(htest.skyTan_likelihood(obs))
			ll2.append(htest2.skyTan_likelihood(obs))

		#ll=htest.skyTan_likelihood(obs)
		import matplotlib.pyplot as plt
		plt.plot(values,ll)
		plt.plot(values,ll2)
		plt.axvline(170)
		plt.show()
		print(h)
	'''

	#Test Disc likelihood
	'''
	if True:
		d=Disc(SR=50,Sz=30,Sphi=30,VR=0,Vz=0,Vphi=220,C_R_z=0, C_R_phi=0, C_z_phi=0)
		print(d)


		from GAstro.rand import  consistent_skypoint_cartesian
		np.random.seed(140)
		coord = consistent_skypoint_cartesian(100)
		Wl,Wb=d.generate_skyTan(coord[0],coord[1],coord[2],coord[3],Npersample=1)
		Vl_err=np.random.uniform(0.1, 10,len(Wl))
		Vb_err=np.random.uniform(0.1, 10,len(Wl))
		C_Vl_Vb = np.zeros_like(Vl_err)
		Wl = np.random.normal(Wl, Vl_err)
		Wb = np.random.normal(Wb, Vb_err)
		obs=Observed(coord[0],coord[1],coord[2],coord[3],  Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

		values = np.linspace(-500,500,3000)
		ll=[]
		ll2=[]
		for val in values:
			dtest = Disc(50, 30, 30, VR=0, Vz=0, Vphi=val, C_R_z=0, C_R_phi=0, C_z_phi=0, label='disc')
			ll.append(dtest.skyTan_likelihood(obs))

		#ll=htest.skyTan_likelihood(obs)
		import matplotlib.pyplot as plt
		plt.plot(values,ll)
		#plt.plot(values,ll2)
		plt.axvline(220)
		plt.show()
	'''

	#Test combined
	'''
	if True:
		d = Disc(SR=50, Sz=30, Sphi=30, VR=10, Vz=-20, Vphi=220, C_R_z=0, C_R_phi=0., C_z_phi=0)
		h=d.to_spherical(70)
		#h = Halo(150, 60, 60, Vr=0, Vtheta=0, Vphi=220, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
		d2=h.to_cylindrical(70)
		print(h)
		print(d2)
	'''


	#Test likelihood
	'''
	if True:
		h=Halo(150, 60, 60, Vr=0, Vtheta=0, Vphi=0,  C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
		from GAstro.rand import  consistent_skypoint_cartesian
		coord = consistent_skypoint_cartesian(10)
		Wl,Wb=h.generate_skyTan(coord[0],coord[1],coord[2],coord[3],Npersample=1)
		Vl_err=np.random.uniform(0.1, 5,len(Wl))
		Vb_err=np.random.uniform(0.1, 5,len(Wl))
		C_Vl_Vb = np.zeros_like(Vl_err)
		Wl = np.random.normal(Wl, Vl_err)
		Wb = np.random.normal(Wb, Vb_err)
		obs=Observed(coord[0],coord[1],coord[2],coord[3],  Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)
		htest = Halo(150, 60, 60, Vr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
		ll=htest.skyTan_likelihood(obs)
		print(ll)
		ll=htest.skyTan_rawlikelihood(obs)
		print(ll.shape)
	'''

	'''
	#Test background
	if True:
		B=SkyTanBackground(100,100,Vl=100,Vb=-200,C_Vl_Vb=-0.8,label='back')
		B2=SkyTanBackground(300,300,Vl=0,Vb=0,C_Vl_Vb=0.,label='back')

		Vl,Vb=B.generate_sample(1000)

		from GAstro.rand import  consistent_skypoint_cartesian
		coord = consistent_skypoint_cartesian(100)

		Wl,Wb=B.generate_skyTan(coord[0],coord[1],coord[2],coord[3],Npersample=1)
		Vl_err=np.random.uniform(0.1, 0.5,len(Wl))
		Vb_err=np.random.uniform(0.1, 0.5,len(Wl))
		C_Vl_Vb = np.zeros_like(Vl_err)
		Wl = np.random.normal(Wl, Vl_err)
		Wb = np.random.normal(Wb, Vb_err)
		obs=Observed(coord[0],coord[1],coord[2],coord[3],  Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)


		import matplotlib.pyplot as plt
		plt.scatter(Wl,Wb)

		print(B.skyTan_likelihood(obs))
		print(B2.skyTan_likelihood(obs))
		print(np.sum(np.log(B2.skyTan_rawlikelihood(obs))))
		print(B2)
		print(B)
		plt.show()
	'''
