from .kinematic import Halo, Giano, Disc, SkyTanBackground
from  .distribution import Uniform, Gaussian, PositiveCauchy, PositiveGaussian, Cauchy, LogNormal
import logging
import  numpy as np
from GAstro.rand import Multivariate
import warnings


#TODO Everything here assume a lef-hand frame of reference (Sun at negative x), however the kinematic part has the possibility to select left or right hand system, that should be possible also for the fit model
def logger(className):
	return logging.getLogger(__name__+':'+className)

class FitModel_Skeleton():

	def __init__(self,param_names=None):

		if param_names is None: param_names=tuple(['param%i'%i for i in range(9)])

		self.var_names = param_names
		self.option_properties={}
		self.option_properties['0']={'Np': 0,   'Names': []} #All fixed
		self.option_properties['A']={'Np': 3,   'Names': self.var_names[3:6]} #Three dispersions
		self.option_properties['AI']={'Np':1,   'Names': self.var_names[3:4]} #One dispersion istropic
		self.option_properties['AT']={'Np':2,   'Names': self.var_names[3:5]} #Radial Disperspion + tangential dispersion (e.g. sigma_theta=sigma_phi)
		self.option_properties['B']={'Np': 4,   'Names': self.var_names[2:6]} #Rotation Velocity + Three dispersion
		self.option_properties['BI']={'Np': 2,   'Names': self.var_names[2:4]} #Rotation Velocity + One dispersion isotropic
		self.option_properties['BT']={'Np': 3,   'Names': self.var_names[2:5]} #Rotation Velocity + Radial Disperspion + tangential dispersion
		self.option_properties['C']={'Np': 6,   'Names': self.var_names[3:]} #Three dispersions + Covariance
		self.option_properties['D']={'Np': 4,   'Names': tuple([self.var_names[0],]+list(self.var_names[3:6]))} #Vradial  + Three dispersion
		self.option_properties['DT']={'Np': 3,   'Names': tuple([self.var_names[0],]+list(self.var_names[3:5]))} #Vradial  + Radial Disperspion + tangential dispersion (e.g. sigma_theta=sigma_phi)
		self.option_properties['DR']={'Np': 5,   'Names': tuple([self.var_names[0],]+list(self.var_names[2:6]))} #Vradial + Rotation + Three dispersion
		self.option_properties['DTR']={'Np': 4,   'Names': tuple([self.var_names[0],]+list(self.var_names[2:5]))} #Vradial + Rotation + Radial Disperspion + tangential dispersion (e.g. sigma_theta=sigma_phi)
		self.option_properties['E']={'Np': 7,   'Names': self.var_names[2:]} #Rotation + Three dispersion + Covariance
		self.option_properties['F']={'Np': 8,   'Names': tuple([self.var_names[0],]+list(self.var_names[2:]))} #Vraidl + ROtation + Three dispersion + Covaraince
		self.option_properties['G']={'Np': 9,   'Names': self.var_names} #Everything

		self.special_cases_isotropic   = ('AI', 'BI')
		self.special_cases_tangential  = ('AT', 'BT','DT','DTR','BT')
		self._allowed = tuple(self.option_properties.keys())
		#Here we are transformig this in a tuple, because in Python 3 the function .keys() is an iterator
		#and iterator cannot be pickled safely. So trying to pickle this class results in a 'can't pickle dict_keys objects' error
		#(see https://cmry.github.io/notes/picklepy3). This can be important for example if we want to use this class in a multiprocessing
		#parallelisation that use pickle.
		self.current_param=dict(zip(self.var_names,np.zeros(len(self.var_names))))
		self.kinematic_model = None


	#set param
	def set_param(self,option,param):
		self._set_param(option,param)
		return param
	def _set_param(self,option,param):

		Np_input=len(param)
		Np_model=self.option_properties[option]['Np']
		param_names=self.option_properties[option]['Names']
		if Np_input!=Np_model: raise ValueError('The length of parameter vector in input (%i) is not the same of the expected number of parameters (%i) for option %s'%(Np_input,Np_model,option))
		set_dic = dict(zip(param_names,param))
		self.current_param.update(set_dic)
		if option in self.special_cases_isotropic: self._set_isotropic()
		elif option in self.special_cases_tangential: self._set_tangential()


		return param

	def is_velocity_dispersion_larger(self, other):
		"""
		Check if all the velocity dispersion of this model are larger thna the other models
		:param other:
		:return:
		"""
		s_names = self.var_names[3:6]
		o_names = other.var_names[3:6]

		for selfname in s_names:
			for othername in o_names:
				if self.current_param[selfname]<other.current_param[othername]: return False

		return True

	def is_Vphi_larger(self, other):
		"""
		Check if  the azimuthal velocity of this models is larger than  other models
		:param other:
		:return: True if Vphi_this > Vphi_other
		"""
		Vphi_this = self.var_names[2]
		Vphi_other = other.var_names[2]

		return Vphi_this>Vphi_other

	def return_model(self, param=None):

		if self.kinematic_model is  None: NotImplementedError('Return Model not implemented yet')
		elif param is not None: self.set_param(param)
		return self.kinematic_model(**self.current_param)

	def loglikelihood(self,observed_matrix,param=None):
		raise NotImplementedError('Likelihood Model not implemented yet')

	def _check_dict_keys(self,dic):

		if all(name in self.var_names for name in dic.keys()): return True

		return False
	def _set_isotropic(self):
		"""
		Fix S2,S3 to the same value of S1
		:return:
		"""
		S1, S2, S3 = self.var_names[3:6]
		S1_now=self.current_param[S1]
		self.current_param.update({S2:S1_now, S3:S1_now})

		return S1_now

	def _set_tangential(self):
		"""
		Fix S3 to the same value of S2
		:return:
		"""
		S2, S3 = self.var_names[4:6]
		S2_now = self.current_param[S2]
		self.current_param.update({S3: S2_now,})

		return S2_now

	def __getitem__(self, name):
		if name in self.var_names: return self.current_param[name]
		else: raise ValueError('Not param %s in this model'%name)

class FitHalo(FitModel_Skeleton):

	def __init__(self, option, prior_range={}, fixed_param={}):

		#Define names and initiliase from super:
		names_list=('Vr','Vtheta','Vphi','Sr','Stheta','Sphi','C_r_theta', 'C_r_phi', 'C_theta_phi')
		super().__init__(param_names=names_list)
		self.clog = logger(self.__class__.__name__)

		#Check if option is valid
		input_option=option.upper()
		if input_option in self._allowed: self.option=input_option
		else: raise NotImplementedError('Fit Option %s not implemented. Implemented options are: %s'%(input_option,str(self._allowed)[1:-1]))

		#Check if prior_range and fixed_param contain invalid names

		#DEFAULT
		prior_range_default = {'Vr': Gaussian(0,500), 'Vtheta': Gaussian(0,500), 'Vphi':  Gaussian(0,500), 'Sr': PositiveCauchy(0,500), 'Stheta': PositiveCauchy(0,500), 'Sphi': PositiveCauchy(0,500),
										'C_r_phi': Uniform(-1,1), 'C_r_theta': Uniform(-1,1), 'C_theta_phi': Uniform(-1,1)}
		fixed_param_default = {'Vr': 0., 'Vtheta':0., 'Vphi':0., 'Sr':100, 'Stheta':100, 'Sphi':100, 'C_r_phi':0, 'C_r_theta':0, 'C_theta_phi':0}

		if self._check_dict_keys(prior_range)==False: raise ValueError('Some of the keys is prior_range are not parameters of this model')
		if self._check_dict_keys(fixed_param)==False: raise ValueError('Some of the keys is fixed_param are not parameters of this model')

		self.this_properties = self.option_properties[self.option]
		self.prior_range = dict(prior_range_default)
		self.prior_range.update(prior_range)
		self.fixed_param = dict(fixed_param_default)
		self.fixed_param.update(fixed_param)
		self.current_param = dict(self.fixed_param)
		self.kinematic_model=Halo


	@property
	def param_names(self):

		return self.this_properties['Names']

	@property
	def Nparam(self):

		return self.this_properties['Np']


	@property
	def current_vdisp(self):

		return (self.current_param['Sr'], self.current_param['Stheta'], self.current_param['Sphi'])

	@property
	def current_vphi(self):

		return self.current_param['Vphi']


	def set_param(self,param):
		self._set_param(self.option,param)
		return param

	def return_model(self, param=None):

		if param is not None: self.set_param(param)
		return self.kinematic_model(**self.current_param)


	def loglikelihood(self,observed_matrix,param=None):
		model=self.return_model(param)
		#check physic model
		if model.is_model_physical: logl = model.skyTan_likelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf

		return logl
	def logprior(self, param=None):

		if param is not None:
			param=self.set_param(param)
		else:
			param = [self.current_param[name] for name in self.this_properties['Names']]

		logprior=0
		for name, value in zip(self.this_properties['Names'],param):

			logprior_f = self.prior_range[name].logprob
			logprior_t = logprior_f(value)
			if np.isfinite(logprior_t): logprior+=logprior_f(value)
			else: logprior=-np.inf

		return logprior
	def logprob(self, observed_matrix, param=None):
		if param is not None: self.set_param(param)

		logprior = self.logprior()
		if np.isfinite(logprior):
			logl=self.loglikelihood(observed_matrix)
			logprob = logprior + logl
		else:
			logl = -np.inf
			logprob = -np.inf

		return logprob, logl, logprior
	def rawlikelihood(self,observed_matrix,param=None):
		model=self.return_model(param)
		#check physic model
		if model.is_model_physical: logl = model.skyTan_rawlikelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf
		return logl

	def sample_from_prior(self, N):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			generate_f = self.prior_range[name].generate
			p0[:, i] = generate_f(N)

		return p0
	def sample_from_current(self, N, gau_eps=0.1):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			mean = self.fixed_param[name]
			p0[:, i] = np.random.normal(mean, gau_eps, N)

		return p0

class FitGiano(FitModel_Skeleton):

	def __init__(self, option, prior_range={}, fixed_param={}):

		#Define names and initiliase from super:
		names_list=('Lr','Vtheta','Vphi','Sr','Stheta','Sphi','C_r_theta', 'C_r_phi', 'C_theta_phi')
		super().__init__(param_names=names_list)
		self.clog = logger(self.__class__.__name__)

		#Check if option is valid
		input_option=option.upper()
		if input_option in self._allowed: self.option=input_option
		else: raise NotImplementedError('Fit Option %s not implemented. Implemented options are: %s'%(input_option,str(self._allowed)[1:-1]))

		#Check if prior_range and fixed_param contain invalid names

		#DEFAULT
		prior_range_default = {'Lr': PositiveCauchy(0,500), 'Vtheta': Gaussian(-500,500), 'Vphi': Gaussian(-500,500), 'Sr': PositiveCauchy(0,500), 'Stheta': PositiveCauchy(0,500), 'Sphi': PositiveCauchy(0,500),
										'C_r_phi': Uniform(-1,1), 'C_r_theta': Uniform(-1,1), 'C_theta_phi': Uniform(-1,1)}
		fixed_param_default = {'Lr': 0., 'Vtheta':0., 'Vphi':0., 'Sr':100, 'Stheta':100, 'Sphi':100, 'C_r_phi':0, 'C_r_theta':0, 'C_theta_phi':0}

		if self._check_dict_keys(prior_range)==False: raise ValueError('Some of the keys is prior_range are not parameters of this model')
		if self._check_dict_keys(fixed_param)==False: raise ValueError('Some of the keys is fixed_param are not parameters of this model')

		self.this_properties = self.option_properties[self.option]
		self.prior_range = dict(prior_range_default)
		self.prior_range.update(prior_range)
		self.fixed_param = dict(fixed_param_default)
		self.fixed_param.update(fixed_param)
		self.current_param = dict(self.fixed_param)
		self.kinematic_model=Giano

	@property
	def param_names(self):

		return self.this_properties['Names']

	@property
	def Nparam(self):

		return self.this_properties['Np']

	@property
	def current_vdisp(self):

		return self.current_param['Sr'], self.current_param['Stheta'], self.current_param['Sphi']

	@property
	def current_vphi(self):

		return self.current_param['Vphi']

	def set_param(self,param):
		self._set_param(self.option,param)
		return param

	def return_model(self, param=None):
		if param is not None: self.set_param(param)

		return self.kinematic_model(**self.current_param)
	def loglikelihood(self,observed_matrix,param=None):
		model=self.return_model(param)
		#check physic model
		if model.is_model_physical: logl = model.skyTan_likelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf

		return logl
	def logprior(self, param=None):

		if param is not None:
			param=self.set_param(param)
		else:
			param = [self.current_param[name] for name in self.this_properties['Names']]

		logprior=0
		for name, value in zip(self.this_properties['Names'],param):

			logprior_f = self.prior_range[name].logprob
			logprior_t = logprior_f(value)
			if np.isfinite(logprior_t): logprior+=logprior_f(value)
			else: logprior=-np.inf

		return logprior
	def logprob(self, observed_matrix, param=None):
		if param is not None: self.set_param(param)

		logprior = self.logprior()
		if np.isfinite(logprior):
			logl=self.loglikelihood(observed_matrix)
			logprob = logprior + logl
		else:
			logl = -np.inf
			logprob = -np.inf

		return logprob, logl, logprior
	def rawlikelihood(self,observed_matrix,param=None):
		model=self.return_model(param)
		#check physic model
		if model.is_model_physical: logl = model.skyTan_rawlikelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf
		return logl

	def sample_from_prior(self, N):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			generate_f = self.prior_range[name].generate
			p0[:, i] = generate_f(N)

		return p0
	def sample_from_current(self, N, gau_eps=0.1):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			mean = self.fixed_param[name]
			p0[:, i] = np.random.normal(mean, gau_eps, N)

		return p0

class FitDisc(FitModel_Skeleton):

	def __init__(self, option, prior_range={}, fixed_param={}):

		#Define names and initiliase from super:
		names_list=('VR','Vz','Vphi','SR','Sz','Sphi','C_R_z', 'C_R_phi', 'C_z_phi')
		super().__init__(param_names=names_list)
		self.clog = logger(self.__class__.__name__)

		#Check if option is valid
		input_option=option.upper()
		if input_option in self._allowed: self.option=input_option
		else: raise NotImplementedError('Fit Option %s not implemented. Implemented options are: %s'%(input_option,str(self._allowed)[1:-1]))

		#Check if prior_range and fixed_param contain invalid names

		#DEFAULT
		prior_range_default = {'VR': Gaussian(0,500), 'Vz': Gaussian(0,500), 'Vphi': Gaussian(500,500), 'SR': PositiveCauchy(0,500), 'Sz': PositiveCauchy(0,500), 'Sphi': PositiveCauchy(0,500),
										'C_R_z': Uniform(-1,1), 'C_R_phi': Uniform(-1,1), 'C_z_phi': Uniform(-1,1)}
		fixed_param_default = {'VR': 0., 'Vz':0., 'Vphi':0., 'SR':100, 'Sz':100, 'Sphi':100, 'C_R_z':0, 'C_R_phi':0, 'C_z_phi':0}

		if self._check_dict_keys(prior_range)==False: raise ValueError('Some of the keys is prior_range are not parameters of this model')
		if self._check_dict_keys(fixed_param)==False: raise ValueError('Some of the keys is fixed_param are not parameters of this model')

		self.this_properties = self.option_properties[self.option]
		self.prior_range = dict(prior_range_default)
		self.prior_range.update(prior_range)
		self.fixed_param = dict(fixed_param_default)
		self.fixed_param.update(fixed_param)
		self.current_param = dict(self.fixed_param)
		self.kinematic_model=Disc




	@property
	def param_names(self):

		return self.this_properties['Names']

	@property
	def Nparam(self):

		return self.this_properties['Np']

	@property
	def current_vdisp(self):

		return self.current_param['SR'], self.current_param['Sz'], self.current_param['Sphi']

	@property
	def current_vphi(self):

		return self.current_param['Vphi']

	def set_param(self,param):
		self._set_param(self.option,param)
		return param

	def return_model(self, param=None):

		if param is not None: self.set_param(param)

		return self.kinematic_model(**self.current_param)
	def loglikelihood(self,observed_matrix,param=None):
		model=self.return_model(param)
		#check physic model
		if model.is_model_physical: logl = model.skyTan_likelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf

		return logl
	def logprior(self, param=None):

		if param is not None:
			param=self.set_param(param)
		else:
			param = [self.current_param[name] for name in self.this_properties['Names']]

		logprior=0
		for name, value in zip(self.this_properties['Names'],param):

			logprior_f = self.prior_range[name].logprob
			logprior_t = logprior_f(value)
			if np.isfinite(logprior_t): logprior+=logprior_f(value)
			else: logprior=-np.inf

		return logprior
	def logprob(self, observed_matrix, param=None):
		if param is not None: self.set_param(param)

		logprior = self.logprior()
		if np.isfinite(logprior):
			logl=self.loglikelihood(observed_matrix)
			logprob = logprior + logl
		else:
			logl = -np.inf
			logprob = -np.inf

		return logprob, logl, logprior
	def rawlikelihood(self,observed_matrix,param=None):
		model=self.return_model(param)
		#check physic model
		if model.is_model_physical: logl = model.skyTan_rawlikelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf
		return logl

	def sample_from_prior(self, N):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			generate_f = self.prior_range[name].generate
			p0[:, i] = generate_f(N)

		return p0
	def sample_from_current(self, N, gau_eps=0.1):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			mean = self.fixed_param[name]
			p0[:, i] = np.random.normal(mean, gau_eps, N)

		return p0

class FitSkyTanBackground(FitModel_Skeleton):

	def __init__(self, option, prior_range={}, fixed_param={}):

		#Define names and initiliase from super:
		self.var_names = ('Vl','Vb','Sl','Sb','C_Vl_Vb')
		self.option_properties={}
		self.option_properties['0']={'Np': 0,   'Names': []} #All fixed
		self.option_properties['A']={'Np': 2,   'Names': self.var_names[2:4]} #Sl, Sb
		self.option_properties['AI']={'Np': 1,   'Names': self.var_names[2:3]} #S=Sl=Sb
		self.option_properties['AM']={'Np': 2,   'Names': self.var_names[2:4]} #Sl, Sb but with Vl, Vb fixed to the mean value
		self.option_properties['AIM']={'Np': 1,   'Names': self.var_names[2:3]} #S=Sl=Sb but with Vl, Vb fixed to the mean value
		self.option_properties['B']={'Np': 2,   'Names': self.var_names[:2]} #Vl, Vb
		self.option_properties['C']={'Np': 3,   'Names': self.var_names[2:]} #Sl, Sb, C
		self.option_properties['CM']={'Np': 3,   'Names': self.var_names[2:]} #Sl, Sb, C but with Vl, Vb fixed to the mean value
		self.option_properties['D']={'Np': 5,   'Names': self.var_names[:]} #Vl, Vb, Sl, Sb, C
		self.option_properties['DI']={'Np': 3,   'Names': self.var_names[:3]} #Vl, Vb, S=Sl=Sb

		self.special_cases_mean=('AM','AIM','CM')
		self.special_cases_isotropic=('AI','DI','AIM')
		self.special_cases_tangential=()

		self._allowed = tuple(self.option_properties.keys())
		#Here we are transformig this in a tuple, because in Python 3 the function .keys() is an iterator
		#and iterator cannot be pickled safely. So trying to pickle this class results in a 'can't pickle dict_keys objects' error
		#(see https://cmry.github.io/notes/picklepy3). This can be important for example if we want to use this class in a multiprocessing
		#parallelisation that use pickle.

		self.clog = logger(self.__class__.__name__)

		#Check if option is valid
		input_option=option.upper()
		if input_option in self._allowed: self.option=input_option
		else: raise NotImplementedError('Fit Option %s not implemented. Implemented options are: %s'%(input_option,str(self._allowed)[1:-1]))

		#Check if prior_range and fixed_param contain invalid names

		#DEFAULT
		prior_range_default = {'Vl': Gaussian(0,500), 'Vb': Gaussian(0,500), 'Sl': PositiveCauchy(0,500), 'Sb': PositiveCauchy(0,500), 'C_Vl_Vb': Uniform(-1,1)}
		fixed_param_default = {'Vl': 0., 'Vb':0., 'Sl':100., 'Sb':100, 'C_Vl_Vb':0}

		if self._check_dict_keys(prior_range)==False: raise ValueError('Some of the keys is prior_range are not parameters of this model')
		if self._check_dict_keys(fixed_param)==False: raise ValueError('Some of the keys is fixed_param are not parameters of this model')

		self.this_properties = self.option_properties[self.option]
		self.prior_range = dict(prior_range_default)
		self.prior_range.update(prior_range)
		self.fixed_param = dict(fixed_param_default)
		self.fixed_param.update(fixed_param)
		self.current_param = dict(self.fixed_param)
		self.kinematic_model=SkyTanBackground

	@property
	def param_names(self):

		return self.this_properties['Names']

	@property
	def Nparam(self):

		return self.this_properties['Np']

	@property
	def current_vdisp(self):

		return self.current_param['Sl'], self.current_param['Sb']

	@property
	def current_vphi(self):

		return -np.inf #SInce it has not a defined vphi, we return always -np.inf so when we compare Vphi this is always lower

	def set_param(self,param):
		self._set_param(self.option,param)
		return param

	def return_model(self, param=None):

		if param is not None: self.set_param(param)

		return self.kinematic_model(**self.current_param)
	def loglikelihood(self,observed_matrix,param=None):

		#First setting of param
		if param is not None: self.set_param(param)
		#Correct param for special cases
		if self.option in self.special_cases_mean: self._set_Vl_Vb(observed_matrix) #Set Vl and Vb to the mean of the observed sample
		model=self.return_model() #Not using param beacuse we are usgin directly self.current_param updated in the row above

		#check physic model
		if model.is_model_physical: logl = model.skyTan_likelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf

		return logl
	def logprior(self, param=None):

		if param is not None:
			param=self.set_param(param)
		else:
			param = [self.current_param[name] for name in self.this_properties['Names']]

		logprior=0
		for name, value in zip(self.this_properties['Names'],param):

			logprior_f = self.prior_range[name].logprob
			logprior_t = logprior_f(value)
			if np.isfinite(logprior_t): logprior+=logprior_f(value)
			else: logprior=-np.inf

		return logprior
	def logprob(self, observed_matrix, param=None):


		logprior = self.logprior(param)
		if np.isfinite(logprior):
			logl=self.loglikelihood(observed_matrix,param)
			logprob = logprior + logl
		else:
			logl = -np.inf
			logprob = -np.inf

		return logprob, logl, logprior
	def rawlikelihood(self,observed_matrix,param=None):

		#First setting of param
		if param is not None: self.set_param(param)
		#Correct param for special cases
		if self.option in self.special_cases_mean: self._set_Vl_Vb(observed_matrix) #Set Vl and Vb to the mean of the observed sample
		model=self.return_model() #Not using param beacuse we are usgin directly self.current_param updated in the row above

		#check physic model
		if model.is_model_physical: logl = model.skyTan_rawlikelihood(observed_matrix=observed_matrix)
		else: logl = -np.inf
		return logl

	def _set_Vl_Vb(self, observed_matrix):

		Vl_mean, Vb_mean = np.mean(observed_matrix.Vl), np.mean(observed_matrix.Vb)

		self.current_param.update({'Vl':Vl_mean,'Vb':Vb_mean})

		return Vl_mean, Vb_mean
	def _set_isotropic(self):
		"""
		Fix Sb the same value of S1
		:return:
		"""

		Sl_now=self.current_param['Sl']
		self.current_param.update({'Sb':Sl_now})

		return Sl_now

	def sample_from_prior(self, N):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			generate_f = self.prior_range[name].generate
			p0[:, i] = generate_f(N)

		return p0
	def sample_from_current(self, N, gau_eps=0.1):

		p0 = np.zeros(shape=(N, self.Nparam))
		for i, name in enumerate(self.param_names):
			mean = self.fixed_param[name]
			p0[:, i] = np.random.normal(mean, gau_eps, N)

		return p0

class FitComponents():

	def __init__(self,*args, prior_range_weights={}):

		self._check_input(*args)
		self.Components=args
		self.Ncomponents = len(self.Components)
		self.Nparams = self._total_number_parameters()
		self.slice_instruction = self._set_slice()
		self._param_names = ()
		self._set_param_names()


	@property
	def param_names(self):

		return self._param_names

	@property
	def Nparam(self): #For consistency with other Fit classes

		return self.Nparams

	def _check_input(self,*args):

		for component in args:
			if isinstance(component,FitModel_Skeleton): pass
			else: raise ValueError('Some of  model(s) in input is(are) not an instance of the Class Poe.fitmodel.FitModel')
	def _set_param_names(self):
		names=['w$_%i$'%(i+1) for i in range(self.Ncomponents)]
		for i,component in enumerate(self.Components):
			for name in component.param_names:
				names.append('%s$_%i$'%(name,i+1))
		self._param_names=tuple(names)

		return names
	def _total_number_parameters(self):

		Nparam=self.Ncomponents
		for model in self.Components:
			Nparam+= model.Nparam

		return Nparam
	def _set_slice(self):

		slice_list = [np.s_[:self.Ncomponents],] #First indec containing the weight
		counter=self.Ncomponents

		for component in self.Components:
			Np_component = component.Nparam
			slice_list.append( np.s_[counter:counter+Np_component] )
			counter+=Np_component

		#posterii check
		if counter!=self.Nparams: raise ValueError('The value of the counter is not the same of the total number of parameters')

		return slice_list
	def _simplexify(self,weights):
		"""
		Generate a K-dimensional uniform Simplex (N values between 0 and 1 and with total sum=1), starting from
		an array of Uniform generated value between 0 an 1. It uses the Exponential sampling method (http://blog.geomblog.org/2005/10/sampling-from-simplex.html).
		:param array: 2D Array with dimension NxK, where N are the number of point we want to generate a simples and K is the simplex dimension.
		:return:  a 2D Array with dimension NxK storing the simplex value obtained from the poin in input. The simplex distribution is uniform
		but this can be used as a domain for a Dirichlet distribution to generate non-uniform simplex samples.
		"""

		if self.Ncomponents==2:
			return np.array([weights[0],1-weights[0]])

		input_arr = np.atleast_1d(weights)
		Wy = -np.log(input_arr)
		Wy = Wy / np.sum(Wy)

		return Wy

	def return_models(self, param):

		model_list=[]
		for component,slice in zip(self.Components,self.slice_instruction[1:]): #Start from 1 to not consider the weights
			model_list.append(component.return_model(param[slice]))

		return model_list
	def loglikelihood(self,observed_matrix,param):

		slices=self.slice_instruction
		#First of all set weights (including prior on the support between 0 and 1)
		#TODO: Add the Dirichlet Prior
		W_raw = np.array(param[slices[0]])
		if np.any((W_raw<=0) | (W_raw>=1)): return -np.inf, np.full(shape=(observed_matrix.N, self.Ncomponents), fill_value=np.nan)
		Wsimplex=self._simplexify(W_raw)

		likely_container=np.zeros(shape=(observed_matrix.N, self.Ncomponents)) #An array that will contain the probability of each star to belong to a given component given the parameters

		i=0
		for weight, component, slice in zip(Wsimplex,self.Components,slices[1:]):

			likel_t = component.rawlikelihood(observed_matrix=observed_matrix, param=param[slice])
			if np.all(np.isfinite(likel_t)):
				likely_container[:,i] = weight*likel_t
			else:
				Q_particles = np.full_like(likely_container,np.nan)
				return -np.inf, Q_particles
			i+=1

		like_per_part     =  np.sum(likely_container,axis=1, keepdims=True)
		Q_particles       =  likely_container/like_per_part

		return np.sum(np.log(like_per_part)), Q_particles
	def logprior(self, param, vdisp_order=None, vphi_order=None):

		slices=self.slice_instruction[1:] #Start from 1, because the prior for the weight are inside loglikelihood
		logprior=0


		###CHECK the vdisp_order and vphi_order before beacuse they are less expensive than estimating the prior
		#Vdisp order
		#check the validity
		if vdisp_order is None: pass
		elif vdisp_order==True:
			warnings.warn('vdisp_order True is depreacated, use ascending, descending or None instead. By default True means descending',DeprecationWarning)
			vdisp_order='descending'
		elif vdisp_order==False:
			warnings.warn('vdisp_order True is depreacated, use ascending, descending or None instead. By default True means None',DeprecationWarning)
			vdisp_order=None
		elif (vdisp_order!='ascending')  and ((vdisp_order!='descending') ):
			raise ValueError('vdisp_order can be only None, ascending or descending, you used %s'%str(vdisp_order))
		if vdisp_order is not None:
			for i in range(self.Ncomponents-1):
				for vdisp_1 in self.Components[i].current_vdisp:
					for vdisp_2 in  self.Components[i+1].current_vdisp:
						if (vdisp_order=='ascending') and (vdisp_1>vdisp_2): return -np.inf
						elif (vdisp_order=='descending') and (vdisp_1<vdisp_2): return -np.inf


		#Vphi order
		#check the validity
		if vphi_order is None: pass
		elif vphi_order==True:
			warnings.warn('vphi_order True is depreacated, use ascending, descending or None instead. By default True means descending',DeprecationWarning)
			vphi_order='descending'
		elif vphi_order==False:
			warnings.warn('vphi_order True is depreacated, use ascending, descending or None instead. By default False means None',DeprecationWarning)
			vphi_order=None
		elif (vphi_order!='ascending')  and ((vphi_order!='descending') ):
			raise ValueError('vphi_order can be only None, ascending or descending, you used %s'%str(vphi_order))
		if vphi_order is not None:
			for i in range(self.Ncomponents-1):
				if (vphi_order=='ascending') and (self.Components[i].current_vphi>self.Components[i+1].current_vphi):
					return -np.inf
				if (vphi_order=='descending') and (self.Components[i].current_vphi<self.Components[i+1].current_vphi):
					#print(self.Components[i].current_vphi,self.Components[i+1].current_vphi)
					return -np.inf



		#PRIOR estimate
		for component, slice in zip(self.Components,slices):
			logprior_t = component.logprior(param[slice])
			if np.isfinite(logprior_t): logprior+=logprior_t
			else: return -np.inf


		return logprior

	def logprob(self,observed_matrix,param, vdisp_order=None, vphi_order=None):

		logprior = self.logprior(param, vdisp_order=vdisp_order, vphi_order=vphi_order)

		if np.isfinite(logprior):
			lnlike, Q_per_particle  =  self.loglikelihood(observed_matrix=observed_matrix,param=param)
			logprob                 =  lnlike + logprior

			beta=[]
			for component,slice in zip(self.Components,self.slice_instruction[1:]):
				model = component.return_model(param[slice])
				beta.append(model.beta)
			beta = np.array(beta)

		else:
			logprob = -np.inf
			Q_per_particle = np.full(shape=(observed_matrix.N, self.Ncomponents), fill_value=np.nan)
			beta = np.full(self.Ncomponents,np.nan)


		return logprob,  Q_per_particle, beta
	def sample_from_prior(self, N):
		"""
		Sample the kinematic parameters from the prior distribution.
		:param N: Number of object to sample.
		:return: A numpy array with dimension NxP where P are the number of free parameters in the model (considering also th weights that are fixed to 0.5).
		"""
		slices = self.slice_instruction[0:]
		p0 = np.zeros(shape=(N, self.Nparam))
		p0[:, slices[0]] = np.random.uniform(0, 1, self.Ncomponents * N).reshape(N, self.Ncomponents)

		for component, slice in zip(self.Components, slices[1:]):
			p0[:, slice] = component.sample_from_prior(N)

		return p0


	def sample_from_current(self, N, gau_eps=0.1):
		"""
		Sample values from the values of the kinematic parameters currently stored in the kinematic models. By default all the weights are 0.5
		:param N: Number of object to sample.
		:param gau_eps: Gaussian std to use to sample the parameters.
		:return: A numpy array with dimension NxP where P are the number of free parameters in the model (considering also th weights that are fixed to 0.5).
		"""
		slices = self.slice_instruction
		p0 = np.zeros(shape=(N, self.Nparam))

		p0[:, slices[0]] = 0.5

		for component, slice in zip(self.Components, slices[1:]):
			p0[:, slice] = component.sample_from_current(N, gau_eps=gau_eps)

		return p0


if __name__=='__main__':

	'''
	fm=FitModel(('Vr','Vtheta','Vphi','Sr','Stheta','Sphi','C_r_theta', 'C_r_phi', 'C_theta_phi'))
	print(fm.current_param)
	fm.set_param('A',[10,20,30])
	print(fm.current_param)
	fm.set_param('B',[1,2,3,4])
	print(fm.current_param)
	fm.set_param('C',[1,2,3,4,5])
	print(fm.current_param)
	fm.set_param('D',[1,2,3,4,5,6,7])
	print(fm.current_param)
	fm.set_param('E',[1,2,3,4,5,6,7,8])
	print(fm.current_param)
	fm.set_param('F',[1,2,3,4,5,6,7,8,9])
	print(fm.current_param)


	t=FitHalo('A',fixed_param={'Vr':-100})
	print(t.fixed_param)
	t.set_param([10,20,30])
	print(t.current_param)
	print(t._check_dict_keys(t.fixed_param))
	print(t._check_dict_keys(t.prior_range))

	t=FitGiano('A',fixed_param={'Lr':-100})
	print(t.fixed_param)
	t.set_param([10,20,30])
	print(t.current_param)
	print(t._check_dict_keys(t.fixed_param))
	print(t._check_dict_keys(t.prior_range))
	print(t['St'])
	'''


	'''
	h = Halo(150, 60, 60, Vr=0, Vtheta=0, Vphi=-150, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed
	coord = consistent_skypoint_cartesian(10)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	fm=FitHalo('B',fixed_param={'Vr':0})
	#fm.set_param([30,10,50])
	vals=np.linspace(-300,300,1000)
	ll=[]
	for  val in vals:
		m=fm.likelihood(observed_matrix=obs,param=[val,130,60,60])
		ll.append(m)
	import matplotlib.pyplot as plt

	plt.plot(vals,ll)
	plt.axvline(-150)
	plt.show()
	'''

	'''
	h = Halo(150, 60, 60, Vr=0, Vtheta=0, Vphi=-150, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed
	coord = consistent_skypoint_cartesian(10)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	fm=FitHalo('BI',fixed_param={'Vr':0})
	m = fm.loglikelihood(observed_matrix=obs, param=[-150, 130])
	print(m)
	m = fm.logprior(param=[-150, 130])
	print(m)
	m=fm.logprob(observed_matrix=obs,param=[-150, 130])

	vals=np.linspace(-300,300,1000)
	ll=[]
	ll2=[]
	ll3=[]
	for  val in vals:
		logp,logl,logpr=fm.logprob(observed_matrix=obs,param=[-150,val])
		ll.append(logp)
		ll2.append(logl)
		ll3.append(logpr)
	import matplotlib.pyplot as plt
	plt.plot(vals,ll)
	#plt.plot(vals,ll2)
	#plt.plot(vals,ll3)
	#plt.axvline(-150)
	print(fm.current_param)
	plt.show()
	'''


	'''
	h = Disc(SR=50, Sz=30, Sphi=30, VR=0, Vz=0, Vphi=220, C_R_z=0, C_R_phi=0, C_z_phi=0)
	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed
	coord = consistent_skypoint_cartesian(10)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	fm=FitDisc('B',fixed_param={'VR':0})
	m = fm.likelihood(observed_matrix=obs, param=[-150, 130, 60, 60])
	print(m)
	m = fm.logprior(param=[-150, 130, 60, 60])
	print(m)
	m=fm.logprob(observed_matrix=obs,param=[-150, 130, 60, 60])

	vals=np.linspace(-300,500,1000)
	ll=[]
	ll2=[]
	ll3=[]
	for  val in vals:
		logp,logl,logpr=fm.logprob(observed_matrix=obs,param=[-150,val,60,60])
		ll.append(logp)
		ll2.append(logl)
		ll3.append(logpr)
		print(val,logp,logl,logpr)
	import matplotlib.pyplot as plt
	plt.plot(vals,ll)
	plt.plot(vals,ll2)
	#plt.plot(vals,ll3)
	#plt.axvline(-150)
	plt.show()
	'''

	'''
	modelHalo = FitHalo('B',fixed_param={'Vr':0})
	print(isinstance(modelHalo,FitModel_Skeleton))
	modelDisc = FitDisc('A')
	print(isinstance(modelDisc, FitModel_Skeleton))
	f=FitComponents(modelHalo,modelDisc)
	print(f.Ncomponents, f.Nparams)

	slicator = f._set_slice()
	a=np.arange(0,10)
	print(a)
	print(a[slicator[0]])
	print(a[slicator[1]])
	print(a[slicator[2]])
	print(f._simplexify([0.5,0.8]))
	models=f.return_models(a)
	print(models[0])
	print(models[1])

	h = Disc(30, 30, 30)
	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed

	np.random.seed(10)
	coord = consistent_skypoint_cartesian(10)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	print('First Component')
	A=f.loglikelihood(obs,[0.0001,0.9999,0,100,100,100,30,30,30])
	l=f.Components[0]
	print(A)
	print(l.loglikelihood(obs))
	h = Halo(100, 100, 100, Vr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
	print(h.skyTan_likelihood(obs))

	print('Second Component')
	A=f.loglikelihood(obs,[0.99999999,0.00000001,0,500,100,100,30,30,30])
	l=f.Components[1]
	print(A)
	print(l.loglikelihood(obs))
	h = Disc(30, 30, 30)
	print(h.skyTan_likelihood(obs))
	print(np.sum(np.log(h.skyTan_rawlikelihood(obs))))
	'''

	'''
	h = Disc(30, 30, 30)
	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed

	np.random.seed(10)
	coord = consistent_skypoint_cartesian(10)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	modelHalo = FitHalo('B',fixed_param={'Vr':0})
	modelDisc = FitDisc('A')
	f=FitComponents(modelHalo,modelDisc)


	print('First Component')
	p=[0.0001,0.9999,0,1000,100,100,30,30,30]
	A=f.loglikelihood(obs,p)
	l=f.Components[0]
	print(A)
	L=l.loglikelihood(obs)
	print(L)
	h = Halo(1000, 100, 100, Vr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
	print(h.skyTan_likelihood(obs))
	logp=f.logprob(obs,p)
	logprior=f.logprior(p)
	print(logp, A, logprior, A+logprior)
	'''

	'''
	h = Disc(30, 30, 30)
	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed

	np.random.seed(10)
	coord = consistent_skypoint_cartesian(1000)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	modelHalo = FitHalo('B',fixed_param={'Vr':0})
	modelDisc = FitDisc('A')
	modelDisc2 = FitDisc('A')
	f=FitComponents(modelHalo,modelDisc,modelDisc2)
	p=[0.5,0.5,0,-1000,100,100,100,30,30,30,30,30,30]

	import time
	t1=time.time()
	A=f.logprob(obs,p)
	t2=time.time()
	print(t2-t1)
	'''

	'''
	modelB = FitSkyTanBackground('AIM',fixed_param={'C_Vl_Vb':-0.3})
	print(modelB.current_param)


	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed
	np.random.seed(10)
	h = Halo(1000, 100, 100, Vr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
	coord = consistent_skypoint_cartesian(1000)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	modelB.logprob(obs,param=[120,])
	print(np.mean(Wl),np.mean(Wb))
	print(modelB.current_param)
	'''

	'''
	h = Disc(30, 30, 30)
	from GAstro.rand import consistent_skypoint_cartesian
	from .matrix import Observed

	np.random.seed(10)
	coord = consistent_skypoint_cartesian(10)
	Wl, Wb = h.generate_skyTan(coord[0], coord[1], coord[2], coord[3], Npersample=1)
	Vl_err = np.random.uniform(0.1, 5, len(Wl))
	Vb_err = np.random.uniform(0.1, 5, len(Wl))
	C_Vl_Vb = np.zeros_like(Vl_err)
	Wl = np.random.normal(Wl, Vl_err)
	Wb = np.random.normal(Wb, Vb_err)
	obs = Observed(coord[0], coord[1], coord[2], coord[3], Wl, Wb, Vl_err, Vb_err, C_Vl_Vb)

	modelHalo = FitHalo('BI',fixed_param={'Vr':0})
	modelDisc = FitDisc('AI')
	modelback = FitSkyTanBackground('AIM')
	f=FitComponents(modelHalo,modelDisc,modelback)
	'''

	'''
	print('First Component')
	p=[0.7,0.01,0.99,0,1000,30,100]
	A=f.loglikelihood(obs,p)
	l=f.Components[0]
	print(A)
	L=l.loglikelihood(obs)
	print(L)
	h = Halo(1000, 1000, 1000, Vr=0, Vtheta=0, Vphi=0, C_r_theta=0, C_r_phi=0, C_theta_phi=0, label='Halo')
	print(h.skyTan_likelihood(obs))
	logp=f.logprob(obs,p)
	logprior=f.logprior(p)
	print(logp, A, logprior, A+logprior)
	print(f._simplexify(p[:3]))
	print('LOGP',logp)

	l = f.Components[0]
	print(l.loglikelihood(obs))
	l = f.Components[1]
	print(l.loglikelihood(obs))
	l = f.Components[2]
	print(l.loglikelihood(obs))
	'''

	'''
	modelDisc = FitDisc('A',prior_range={'SR':PositiveGaussian(0,500)})
	modelHalo = FitHalo('AI',prior_range={'Sr':PositiveGaussian(0,20)})
	modelSky  = FitSkyTanBackground('AI')
	modelTotal = FitComponents(modelDisc,modelHalo,modelSky)
	print( modelDisc.sample_from_prior(10))
	print( modelDisc.sample_from_current(10))
	print( modelHalo.sample_from_prior(10))
	print( modelHalo.sample_from_current(10,gau_eps=10))

	print( modelTotal.sample_from_prior(10).shape)
	print( modelTotal.sample_from_current(10))
	'''
